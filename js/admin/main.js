function isJSON(str) {
    try {
        return (JSON.parse(str) && !!str);
    } catch (e) {
        return false;
    }
}

$(document).ready(function(){
	$('input[type=submit]').click(function(event){
		event.preventDefault();
		return false;
	})
});

function exit(){
	$.ajax({
		type: 'POST',
		url: '/php/switch.php',
		data: {
			headerLocation: 'exit'
		},
		success: function(html) {
			if (html.indexOf('success') != -1) {

				location.reload(true);

			}
		}
	});
}
