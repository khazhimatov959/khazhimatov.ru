$(document).ready(function(){

    var html_text = '<div class="statistic_info_line flex statictics_up">' +
                    '    <span class="statistic_info_icon statictics_icon">' +
                    '    <img src="/img/arrow.png" alt="">' +
                    '    </span>' +
                    '    <span class="statistic_info_text">' +
                    '    <p>Количество отданных комментариев</p>' +
                    '    </span>' +
                    '</div>' +
                    
                    '<div class="statistic_info_line flex statictics_down">' +
                    '    <span class="statistic_info_icon statictics_icon">' +
                    '    <img src="/img/arrow.png" alt="">' +
                    '    </span>' +
                    '    <span class="statistic_info_text">' +
                    '    <p>Количество полученных комментариев</p>' +
                    '    </span>' +
                    '</div> ' +
                    
                    '<div class="statistic_info_line flex statictics_up">' +
                    '    <span class="statistic_info_icon statictics_icon">' +
                    '    <img src="/img/rating.png" alt="">' +
                    '    </span>' +
                    '    <span class="statistic_info_text">' +
                    '    <p>Место в рейтинге</p>' +
                    '    </span>' +
                    '</div>' +
                    '<hr>' +
                    '<p class="statistic_text_info">Раздай больше комментариев, повысь свое место в ситеме, получи бонус</p>'                    
                    ;

    tooltip('#statictics_info_icon', html_text, 15, -210, 255, 138);
});
