function show_menu(){ 
	$('.menu_up').toggleClass(function() {
		  if ( $( this ).is( ".down_animation" ) ) {
		  	$(this).removeClass('down_animation');
		    return "up_animation";
		  } else {
		  	$(this).removeClass('up_animation');
		    return "down_animation";
		  }
	} );
}

function exit(){
	$.ajax({
		type: 'POST',
		url: '/php/switch.php',
		data: {
			headerLocation: 'exit'
		},
		success: function(html) {
			if (html.indexOf('success') != -1) {

				location.reload(true);

			}
		}
	});
}
