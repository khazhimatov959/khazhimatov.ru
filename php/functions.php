<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

function check($param, $success = 'success', $false = 'error'){
	if ($param) {
		return $success;
	}
	return $false;
}

function checked($param){
	return check($param, 'checked', '');
}
	
function LoadRequests(){

	global $mysqli;

	$user_id = $_SESSION['user_id_commenter'];
	$query = $mysqli->query("SET SESSION group_concat_max_len = 100000;");
	
	$sql = "SELECT `request`.`id`, `amount`, `link`, `request`.`date`, count(DISTINCT `user_comm`.`account`) as `amount_comm`, GROUP_CONCAT( CONCAT(`user_comm`.`account`, '959--959', `user_comm`.`text`, '959--959', `user_comm`.`date_comm`) SEPARATOR '959,959' ) as `comm` FROM `request` LEFT JOIN `user_comm` ON `request`.`id` = `user_comm`.`id_request` WHERE `author` = '$user_id' GROUP BY `request`.`id` ORDER BY `request`.`date` DESC LIMIT 30";
	$query = $mysqli->query($sql);

	$requests = [];

	while($row = $query->fetch_assoc()){

		$comm_array = [];

		if ($row['comm']) {

			$comm = explode('959,959', $row['comm']);

			for ($i=0; $i < count($comm); $i++) {

				if (!$comm[$i]) continue;

				$explode = explode('959--959', $comm[$i]); 

				$profile = $explode[0];
				$text = $explode[1];
				$date = $explode[2];

				if (!array_key_exists($profile, $comm_array)) {
						$comm_array[$profile] = [];
				}
			
				$comm_array[$profile][] = array('text' => $text, 'date' => $date);

			}

		}

		$requests[] = array('id' => $row['id'], 'amount' => $row['amount'], 'link' => $row['link'], 'date' => $row['date'], 'amount_comm' => $row['amount_comm'], 'comm' => $comm_array);

	}

	return $requests;
}

function checkTariff(){

	global $mysqli;

	$user_id = $_SESSION['user_id_commenter'];

	$sql = "SELECT `tariff_price`.`day`, `tariff_price`.`price`, `date_create` FROM `user_tariff` INNER JOIN `tariff_price` ON `user_tariff`.`tariff_price` = `tariff_price`.`id` WHERE `user` = '$user_id' ORDER BY `date_create` DESC LIMIT 1";	

	$query = $mysqli->query($sql);
	$row = $query->fetch_array();

	$day = $row['day'];
	$price = $row['price'];
	$date_create = $row['date_create'];

	date_default_timezone_set('Europe/Moscow');

	$date_time = strtotime($date_create . " +$day day");
	$cur_time = strtotime(date('Y-m-d H:i:s'));

	if ($date_time < $cur_time) {
		return false;
	}
	return true;

}

function myBalance(){

	global $mysqli;

	$user_id = $_SESSION['user_id_commenter'];
	$sql = "SELECT `balance` FROM `user` WHERE `id` = '$user_id'";
	$query = $mysqli->query($sql);

	$row = $query->fetch_array();
	$balance = $row['balance'];

	return $balance;
}

function myRating(){

	global $mysqli;

	$user_id = $_SESSION['user_id_commenter'];
	
	$sql = "
			SET @meComm = (
							SELECT COUNT(DISTINCT `user`) 
								
								FROM `user_comm` inner join `request` ON `request`.`id` = `user_comm`.`id_request` 
								
								WHERE `request`.`author` = '$user_id' and `user_comm`.`user` != '$user_id'
							)
							
			,

			@myComm = (SELECT COUNT(DISTINCT `id_request`) FROM `user_comm` WHERE `user` = '$user_id')
			
			,

			@place = (
						SELECT COUNT(`user`) + 1 

							FROM (
									SELECT `user`, COUNT(DISTINCT `id_request`) AS `count_comm` 
									FROM `user_comm` GROUP BY `user`
								) 
								AS `rating` 
									
							WHERE `rating`.`count_comm` > @myComm					
					  );	
			";

	$mysqli->query($sql);

	$sql = "SELECT @place AS `place`, @myComm AS `myComm`, @meComm AS `meComm`;";

	$query = $mysqli->query($sql);

	$row = $query->fetch_array();

	$place = $row['place'];
	$myComm = $row['myComm'];
	$meComm = $row['meComm'];

	return array($place, $myComm, $meComm);
}

?>