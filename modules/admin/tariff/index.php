<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);



?>
<link rel="stylesheet" href="/modules/admin/tariff/style/tariff.css">
<div class="tariff_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Тарифы</p>
		</div>
         <div class="main_block_tariff">

             <div class="line">
                 <button class="button_operation">+ Добавить тариф</button>
             </div>

			<div class="table">
							
					<div class="hbody">
						<div class="rows flex">
                            <div class="column"><input type="checkbox"></div>
							<div class="column">Название тарифа</div>
							<div class="column">Описание тарифа</div>
							<div class="column">Дата создания</div>
							<div class="column">ID</div>
							<div class="column"></div>
						</div>
					</div>
							
					<div class="tbody">
						<?php

						// for ($i=0; $i < count($ratings); $i++) { 
						// 		$user = $ratings[$i]['user'];
						// 		$place = $ratings[$i]['place'];
						// 		$count_tariff = $ratings[$i]['count_tariff'];
				
						?> 
							<div class="rows content">
								
								<div class="row row_1 flex">
                                    <div class="column"><input type="checkbox"></div>
									<div class="column">Free</div>
									<div class="column">Получаешь максимум 10 комментариев в день на 1 профиль</div>
                                    <div class="column">14.06.2018</div>
									<div class="column">44554855685</div>
									<div class="column flex operation">
                                        <div class="element_delete">
                                            <img src="/img/delete.png" alt="">
                                        </div>
                                    </div>
								</div>
								
							</div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">Free</div>
                                <div class="column">Получаешь максимум 10 комментариев в день на 1 профиль</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">Free</div>
                                <div class="column">Получаешь максимум 10 комментариев в день на 1 профиль</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">Free</div>
                                <div class="column">Получаешь максимум 10 комментариев в день на 1 профиль</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

						<?php //} ?>

					</div>
							
				</div>

            <div class="line operation_block flex">
                <select name="" id="">
                    <option value="0">Удалить</option>
                    <option value="1">Принять</option>
                </select>
                <button>Принять</button>
            </div>

		</div>	

	</div>

</div>
<script src="/modules/admin/tariff/js/tariff.js"></script>