<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);



?>
<link rel="stylesheet" href="/modules/admin/comments/style/comments.css">
<div class="comment_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">КОММЕНТАРИИ</p>
		</div>
			
		<div class="main_block_comment">

            <div class="line">
                <div class="count_comm flex">
                    <div class="count_comm_text">
                        <p class="title">Всего комментариев в системе</p>
                    </div>
                    <div class="count_comm_number title">
                        <p>10287</p>
                    </div>
                </div>
            </div>

            <div class="line line_count flex">

               <div class="block_count">
                   <div class="block_count_text title">
                       <p>Новые комментарии</p>
                   </div>
                   <div class="block_count_number">
                       <p>1200</p>
                   </div>
               </div>

                <div class="block_count">
                    <div class="block_count_text title">
                        <p>Входящие комментарии</p>
                    </div>
                    <div class="block_count_number">
                        <p>1200</p>
                    </div>
                </div>

                <div class="block_count">
                    <div class="block_count_text title">
                        <p>Исходящие комментарии</p>
                    </div>
                    <div class="block_count_number">
                        <p>1200</p>
                    </div>
                </div>

                <div class="block_count">
                    <div class="block_count_text title">
                        <p>Проблемные (отклоненные)</p>
                    </div>
                    <div class="block_count_number">
                        <p>1200</p>
                    </div>
                </div>

                <div class="block_count">
                    <div class="block_count_text title">
                        <p>Архив</p>
                    </div>
                    <div class="block_count_number">
                        <p>1200</p>
                    </div>
                </div>

            </div>

			
			<div class="table">
							
					<div class="hbody">
						<div class="rows flex">
                            <div class="column"><input type="checkbox"></div>
							<div class="column">№</div>
							<div class="column">Ссылка на пост</div>
							<div class="column">Тип</div>
							<div class="column">Создал</div>
							<div class="column">Выполняет</div>
							<div class="column">Дата создания</div>
							<div class="column">Комме-й написан</div>
							<div class="column">ID</div>
							<div class="column">Действие</div>
						</div>
					</div>
							
					<div class="tbody">
						<?php

						// for ($i=0; $i < count($ratings); $i++) { 
						// 		$user = $ratings[$i]['user'];
						// 		$place = $ratings[$i]['place'];
						// 		$count_comm = $ratings[$i]['count_comm'];
				
						?> 
							<div class="rows content">
								
								<div class="row row_1 flex">
                                    <div class="column"><input type="checkbox"></div>
									<div class="column">1</div>
									<div class="column"><a href="https://www.instagram.com/p/Bl4HgYEDaNV/">https://instagram.com/p/Bl4HgYEDaNV/</a></div>
									<div class="column">Входящие</div>
									<div class="column">usermail@mail.ru</div>
									<div class="column">mymail@mail.ru</div>
									<div class="column">14.06.2018 | 12:34</div>
									<div class="column table_time">14.06.2018 | 12:34</div>
									<div class="column">44554855685</div>
									<div class="column flex operation">
                                        <div class="element_delete">
                                            <img src="/img/success.png" alt="">
                                        </div>
                                        <div class="element_delete">
                                            <img src="/img/delete.png" alt="">
                                        </div>
                                    </div>
								</div>
								
								<hr>

								<div class="row row_2 flex">
									<div class="column title"><!-- <?=$place?> -->Текст: </div>
									<div class="column">
										<p>Отличное качество. где бы я не покупала вещи, вы действительно стараетесь следовать за качеством в мировом смысле этого слова, очень приятно!!!!</p>
									</div>
									<hr>
									<div class="column"><!-- <?=$place?> <-->
										<p class="table_time_title">Осталось до окончания проверки</p><div class="table_time"><p>02:15</p></div>
									</div>
								</div>
								
							</div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">1</div>
                                <div class="column"><a href="https://www.instagram.com/p/Bl4HgYEDaNV/">https://instagram.com/p/Bl4HgYEDaNV/</a></div>
                                <div class="column">Входящие</div>
                                <div class="column">usermail@mail.ru</div>
                                <div class="column">mymail@mail.ru</div>
                                <div class="column">14.06.2018 | 12:34</div>
                                <div class="column table_time">14.06.2018 | 12:34</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/success.png" alt="">
                                    </div>
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row row_2 flex">
                                <div class="column title"><!-- <?=$place?> -->Текст: </div>
                                <div class="column">
                                    <p>Отличное качество. где бы я не покупала вещи, вы действительно стараетесь следовать за качеством в мировом смысле этого слова, очень приятно!!!!</p>
                                </div>
                                <hr>
                                <div class="column"><!-- <?=$place?> <-->
                                    <p class="table_time_title">Осталось до окончания проверки</p><div class="table_time"><p>02:15</p></div>
                                </div>
                            </div>

                        </div>
                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">1</div>
                                <div class="column"><a href="https://www.instagram.com/p/Bl4HgYEDaNV/">https://instagram.com/p/Bl4HgYEDaNV/</a></div>
                                <div class="column">Входящие</div>
                                <div class="column">usermail@mail.ru</div>
                                <div class="column">mymail@mail.ru</div>
                                <div class="column">14.06.2018 | 12:34</div>
                                <div class="column table_time">14.06.2018 | 12:34</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/success.png" alt="">
                                    </div>
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row row_2 flex">
                                <div class="column title"><!-- <?=$place?> -->Текст: </div>
                                <div class="column">
                                    <p>Отличное качество. где бы я не покупала вещи, вы действительно стараетесь следовать за качеством в мировом смысле этого слова, очень приятно!!!!</p>
                                </div>
                                <hr>
                                <div class="column"><!-- <?=$place?> <-->
                                    <p class="table_time_title">Осталось до окончания проверки</p><div class="table_time"><p>02:15</p></div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">1</div>
                                <div class="column"><a href="https://www.instagram.com/p/Bl4HgYEDaNV/">https://instagram.com/p/Bl4HgYEDaNV/</a></div>
                                <div class="column">Входящие</div>
                                <div class="column">usermail@mail.ru</div>
                                <div class="column">mymail@mail.ru</div>
                                <div class="column">14.06.2018 | 12:34</div>
                                <div class="column table_time">14.06.2018 | 12:34</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/success.png" alt="">
                                    </div>
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row row_2 flex">
                                <div class="column title"><!-- <?=$place?> -->Текст: </div>
                                <div class="column">
                                    <p>Отличное качество. где бы я не покупала вещи, вы действительно стараетесь следовать за качеством в мировом смысле этого слова, очень приятно!!!!</p>
                                </div>
                                <hr>
                                <div class="column"><!-- <?=$place?> <-->
                                    <p class="table_time_title">Осталось до окончания проверки</p><div class="table_time"><p>02:15</p></div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">1</div>
                                <div class="column"><a href="https://www.instagram.com/p/Bl4HgYEDaNV/">https://instagram.com/p/Bl4HgYEDaNV/</a></div>
                                <div class="column">Входящие</div>
                                <div class="column">usermail@mail.ru</div>
                                <div class="column">mymail@mail.ru</div>
                                <div class="column">14.06.2018 | 12:34</div>
                                <div class="column table_time">14.06.2018 | 12:34</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/success.png" alt="">
                                    </div>
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row row_2 flex">
                                <div class="column title"><!-- <?=$place?> -->Текст: </div>
                                <div class="column">
                                    <p>Отличное качество. где бы я не покупала вещи, вы действительно стараетесь следовать за качеством в мировом смысле этого слова, очень приятно!!!!</p>
                                </div>
                                <hr>
                                <div class="column"><!-- <?=$place?> <-->
                                    <p class="table_time_title">Осталось до окончания проверки</p><div class="table_time"><p>02:15</p></div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">1</div>
                                <div class="column"><a href="https://www.instagram.com/p/Bl4HgYEDaNV/">https://instagram.com/p/Bl4HgYEDaNV/</a></div>
                                <div class="column">Входящие</div>
                                <div class="column">usermail@mail.ru</div>
                                <div class="column">mymail@mail.ru</div>
                                <div class="column">14.06.2018 | 12:34</div>
                                <div class="column table_time">14.06.2018 | 12:34</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/success.png" alt="">
                                    </div>
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row row_2 flex">
                                <div class="column title"><!-- <?=$place?> -->Текст: </div>
                                <div class="column">
                                    <p>Отличное качество. где бы я не покупала вещи, вы действительно стараетесь следовать за качеством в мировом смысле этого слова, очень приятно!!!!</p>
                                </div>
                                <hr>
                                <div class="column"><!-- <?=$place?> <-->
                                    <p class="table_time_title">Осталось до окончания проверки</p><div class="table_time"><p>02:15</p></div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">1</div>
                                <div class="column"><a href="https://www.instagram.com/p/Bl4HgYEDaNV/">https://instagram.com/p/Bl4HgYEDaNV/</a></div>
                                <div class="column">Входящие</div>
                                <div class="column">usermail@mail.ru</div>
                                <div class="column">mymail@mail.ru</div>
                                <div class="column">14.06.2018 | 12:34</div>
                                <div class="column table_time">14.06.2018 | 12:34</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/success.png" alt="">
                                    </div>
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row row_2 flex">
                                <div class="column title"><!-- <?=$place?> -->Текст: </div>
                                <div class="column">
                                    <p>Отличное качество. где бы я не покупала вещи, вы действительно стараетесь следовать за качеством в мировом смысле этого слова, очень приятно!!!!</p>
                                </div>
                                <hr>
                                <div class="column"><!-- <?=$place?> <-->
                                    <p class="table_time_title">Осталось до окончания проверки</p><div class="table_time"><p>02:15</p></div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">1</div>
                                <div class="column"><a href="https://www.instagram.com/p/Bl4HgYEDaNV/">https://instagram.com/p/Bl4HgYEDaNV/</a></div>
                                <div class="column">Входящие</div>
                                <div class="column">usermail@mail.ru</div>
                                <div class="column">mymail@mail.ru</div>
                                <div class="column">14.06.2018 | 12:34</div>
                                <div class="column table_time">14.06.2018 | 12:34</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/success.png" alt="">
                                    </div>
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row row_2 flex">
                                <div class="column title"><!-- <?=$place?> -->Текст: </div>
                                <div class="column">
                                    <p>Отличное качество. где бы я не покупала вещи, вы действительно стараетесь следовать за качеством в мировом смысле этого слова, очень приятно!!!!</p>
                                </div>
                                <hr>
                                <div class="column"><!-- <?=$place?> <-->
                                    <p class="table_time_title">Осталось до окончания проверки</p><div class="table_time"><p>02:15</p></div>
                                </div>
                            </div>

                        </div>



						<?php //} ?>

					</div>
							
				</div>

            <div class="line operation_block flex">
                <select name="" id="">
                    <option value="0">Удалить</option>
                    <option value="1">Принять</option>
                </select>
                <button>Применить</button>
            </div>

            <div class="line">
                <div class="block_page flex">
                    <div class="block_number flex">
                        <div class="page_number arrow_left">
                            <img src="/img/arrow_up.png" alt="">
                        </div>
                        <div class="page_number">
                            <p>1</p>
                        </div>
                        <div class="page_number">
                            <p>2</p>
                        </div>
                        <div class="page_number">
                            <p>3</p>
                        </div>
                        <div class="page_number">
                            <p>...</p>
                        </div>
                        <div class="page_number">
                            <p>5</p>
                        </div>
                        <div class="page_number arrow_right">
                            <img src="/img/arrow_up.png" alt="">
                        </div>
                    </div>
                    <div class="block_count_page">
                        <div class="text title">
                            Показывать по
                        </div>
                        <select name="" id="">
                            <option value="20">20</option>
                            <option value="40">40</option>
                            <option value="60">60</option>
                            <option value="80">80</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>

		</div>	

	</div>

</div>
<script src="/modules/admin/comments/js/comments.js"></script>