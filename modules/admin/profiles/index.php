<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);



?>
<link rel="stylesheet" href="/modules/admin/profiles/style/profiles.css">
<div class="profiles_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Профили</p>
		</div>
         <div class="main_block_profiles">

             <div class="line flex">
                 <button class="button_operation">Активные</button>
                 <button class="button_operation">На проверке</button>
                 <button class="button_operation">+ Добавить профиль</button>
             </div>

			<div class="table">
							
					<div class="hbody">
						<div class="rows flex">
							<div class="column">Прикрепленный аккаунт</div>
							<div class="column">Пользователь</div>
							<div class="column">Дата оплаты</div>
							<div class="column">Способ оплаты</div>
							<div class="column">Статус</div>
                            <div class="column">Сумма</div>
                            <div class="column">ID</div>
						</div>
					</div>
							
					<div class="tbody">
						<?php

						// for ($i=0; $i < count($ratings); $i++) { 
						// 		$user = $ratings[$i]['user'];
						// 		$place = $ratings[$i]['place'];
						// 		$count_profiles = $ratings[$i]['count_profiles'];
				
						?> 
							<div class="rows content">
								
								<div class="row row_1 flex">
                                    <div class="column">100</div>
									<div class="column">usermaol@mail.ru</div>
									<div class="column">14.06.2018 | 12:38</div>
                                    <div class="column">Банковская карта</div>
                                    <div class="column">Оплачен</div>
                                    <div class="column">5000 руб</div>
									<div class="column">44554855685</div>
								</div>
								
							</div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">100</div>
                                <div class="column">usermaol@mail.ru</div>
                                <div class="column">14.06.2018 | 12:38</div>
                                <div class="column">Банковская карта</div>
                                <div class="column">Оплачен</div>
                                <div class="column">5000 руб</div>
                                <div class="column">44554855685</div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">100</div>
                                <div class="column">usermaol@mail.ru</div>
                                <div class="column">14.06.2018 | 12:38</div>
                                <div class="column">Банковская карта</div>
                                <div class="column">Оплачен</div>
                                <div class="column">5000 руб</div>
                                <div class="column">44554855685</div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">100</div>
                                <div class="column">usermaol@mail.ru</div>
                                <div class="column">14.06.2018 | 12:38</div>
                                <div class="column">Банковская карта</div>
                                <div class="column">Оплачен</div>
                                <div class="column">5000 руб</div>
                                <div class="column">44554855685</div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">100</div>
                                <div class="column">usermaol@mail.ru</div>
                                <div class="column">14.06.2018 | 12:38</div>
                                <div class="column">Банковская карта</div>
                                <div class="column">Оплачен</div>
                                <div class="column">5000 руб</div>
                                <div class="column">44554855685</div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">100</div>
                                <div class="column">usermaol@mail.ru</div>
                                <div class="column">14.06.2018 | 12:38</div>
                                <div class="column">Банковская карта</div>
                                <div class="column">Оплачен</div>
                                <div class="column">5000 руб</div>
                                <div class="column">44554855685</div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">100</div>
                                <div class="column">usermaol@mail.ru</div>
                                <div class="column">14.06.2018 | 12:38</div>
                                <div class="column">Банковская карта</div>
                                <div class="column">Оплачен</div>
                                <div class="column">5000 руб</div>
                                <div class="column">44554855685</div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">100</div>
                                <div class="column">usermaol@mail.ru</div>
                                <div class="column">14.06.2018 | 12:38</div>
                                <div class="column">Банковская карта</div>
                                <div class="column">Оплачен</div>
                                <div class="column">5000 руб</div>
                                <div class="column">44554855685</div>
                            </div>

                        </div>

						<?php //} ?>

					</div>

                <div class="line operation_block flex">
                    <select name="" id="">
                        <option value="0">Удалить</option>
                        <option value="1">Принять</option>
                    </select>
                    <button>Применить</button>
                </div>

                <div class="line">
                    <div class="block_page flex">
                        <div class="block_number flex">
                            <div class="page_number arrow_left">
                                <img src="/img/arrow_up.png" alt="">
                            </div>
                            <div class="page_number">
                                <p>1</p>
                            </div>
                            <div class="page_number">
                                <p>2</p>
                            </div>
                            <div class="page_number">
                                <p>3</p>
                            </div>
                            <div class="page_number">
                                <p>...</p>
                            </div>
                            <div class="page_number">
                                <p>5</p>
                            </div>
                            <div class="page_number arrow_right">
                                <img src="/img/arrow_up.png" alt="">
                            </div>
                        </div>
                        <div class="block_count_page">
                            <div class="text title">
                                Показывать по
                            </div>
                            <select name="" id="">
                                <option value="20">20</option>
                                <option value="40">40</option>
                                <option value="60">60</option>
                                <option value="80">80</option>
                                <option value="100">100</option>
                            </select>
                        </div>
                    </div>
                </div>
							
				</div>

		</div>	

	</div>

</div>
<script src="/modules/admin/profiles/js/profiles.js"></script>