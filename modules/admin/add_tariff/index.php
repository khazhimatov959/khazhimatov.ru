<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);



?>
<link rel="stylesheet" href="/modules/admin/add_tariff/style/add_tariff.css">
<div class="add_tariff_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Добавление тарифа</p>
		</div>
         <div class="main_block_add_tariff">

             <div class="links">
                 <div class="line flex">
                     <p class="title">ID</p><input type="text" value="433354353454" disabled>
                 </div>

                 <div class="line flex">
                     <p class="title">Пользователь</p><input type="text" value="khazhimatov959@mail.ru" disabled>
                 </div>

                 <div class="line flex">
                     <p class="title">Статус профиля</p>
                        <select name="state" id="state">
                            <option value="">Активный</option>
                            <option value="">Неактивный</option>
                        </select>
                 </div>

                 <div class="line flex">
                     <p class="title">Ссылка на аккаунт</p><input type="text" value="" placeholder="Введите имя аккаунта без \"@\"" disabled>
                 </div>

                 <div class="line flex">
                     <p class="title">Назначение</p>
                     <select name="state" id="state">
                         <option value="">Для получения комментариев</option>
                         <option value="">Для принятия комментариев</option>
                     </select>
                 </div>
             </div>

             <div class="line_buttons flex">
                 <button class="button_operation">Созранить</button>
                 <button class="button_operation">Отменить</button>
                 <button class="button_operation">Удалить</button>
             </div>

		</div>	

	</div>

</div>
<script src="/modules/admin/add_tariff/js/add_tariff.js"></script>