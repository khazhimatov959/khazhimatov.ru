<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);



?>
<link rel="stylesheet" href="/modules/admin/ratings/style/ratings.css">
<div class="ratings_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Управление рейтингом</p>
		</div>
         <div class="main_block_ratings">

             <div class="links">
                 <div class="line flex">
                     <p class="title">Дата обновления рейтинга</p><input type="datetime-local" value="2018-06-14T12:38" disabled>
                 </div>

                 <div class="line flex">
                     <p class="title">Краткое описание</p><textarea name="" id="" cols="30" rows="10" disabled>В данном разделе показыны пользователи с наибольшем количеством разданных коментариев.
Таблица обновляется каждые 6 месяцев</textarea>
                 </div>

             </div>

			<div class="table">
							
					<div class="hbody">
						<div class="rows flex">
                            <div class="column">Логин</div>
                            <div class="column">Активность</div>
                            <div class="column">Имя</div>
                            <div class="column">Фамилия</div>
                            <div class="column">Количество комментариев</div>
                            <div class="column">Последняя авторизация</div>
                            <div class="column">ID</div>
						</div>
					</div>
							
					<div class="tbody">
						<?php

						// for ($i=0; $i < count($ratings); $i++) { 
						// 		$user = $ratings[$i]['user'];
						// 		$place = $ratings[$i]['place'];
						// 		$count_ratings = $ratings[$i]['count_ratings'];
				
						?>
                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">khazhimatov959@mail.ru</div>
                                <div class="column">Да</div>
                                <div class="column">Иван</div>
                                <div class="column">Иванов</div>
                                <div class="column">8000</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">khazhimatov959@mail.ru</div>
                                <div class="column">Да</div>
                                <div class="column">Иван</div>
                                <div class="column">Иванов</div>
                                <div class="column">8000</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">khazhimatov959@mail.ru</div>
                                <div class="column">Да</div>
                                <div class="column">Иван</div>
                                <div class="column">Иванов</div>
                                <div class="column">8000</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">khazhimatov959@mail.ru</div>
                                <div class="column">Да</div>
                                <div class="column">Иван</div>
                                <div class="column">Иванов</div>
                                <div class="column">8000</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">khazhimatov959@mail.ru</div>
                                <div class="column">Да</div>
                                <div class="column">Иван</div>
                                <div class="column">Иванов</div>
                                <div class="column">8000</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column">khazhimatov959@mail.ru</div>
                                <div class="column">Да</div>
                                <div class="column">Иван</div>
                                <div class="column">Иванов</div>
                                <div class="column">8000</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

						<?php //} ?>

					</div>
							
				</div>

             <div class="line">
                 <div class="block_page flex">
                     <div class="block_number flex">
                         <div class="page_number arrow_left">
                             <img src="/img/arrow_up.png" alt="">
                         </div>
                         <div class="page_number">
                             <p>1</p>
                         </div>
                         <div class="page_number">
                             <p>2</p>
                         </div>
                         <div class="page_number">
                             <p>3</p>
                         </div>
                         <div class="page_number">
                             <p>...</p>
                         </div>
                         <div class="page_number">
                             <p>5</p>
                         </div>
                         <div class="page_number arrow_right">
                             <img src="/img/arrow_up.png" alt="">
                         </div>
                     </div>
                     <div class="block_count_page">
                         <div class="text title">
                             Показывать по
                         </div>
                         <select name="" id="">
                             <option value="20">20</option>
                             <option value="40">40</option>
                             <option value="60">60</option>
                             <option value="80">80</option>
                             <option value="100">100</option>
                         </select>
                     </div>
                 </div>
             </div>

		</div>	

	</div>

</div>
<script src="/modules/admin/ratings/js/ratings.js"></script>