<?php
session_start();

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/instagramFunctions.php";

if (isset($_POST['headerLocation'])) {
	$headerLocation = $_POST['headerLocation'];
}

if(!isset($headerLocation)){
	$headerLocation = '';
}

switch ($headerLocation) {

	case 'comm_complete':

		$id = $_POST['id'];
		comm_complete($id);

		break;

}

function comm_complete($id){

	global $mysqli;
	$user_id = $_SESSION['user_id_commenter'];

	$answer = array('state' => 'success', 'message' => 'Ваш комментарий засчитан!');

	$sql = "SELECT `login` FROM `profile` WHERE `user` = '$user_id'";
	$query = $mysqli->query($sql);

	if ($row = $query->fetch_array()) {
		$instagram_name = $row['login'];
	}
	else {
		$answer['state'] = 'error';
		$answer['message'] = 'Не обнаружено профиля для раздачи!';
	}

	$sql = "SELECT `media_id` FROM `request` WHERE `id` = '$id'";
	$query = $mysqli->query($sql);

	if ($row = $query->fetch_array()) {
		
		$media_id = $row['media_id'];

		$array_comments = LoadUserComments($instagram_name, $media_id);

		$sql = "SELECT `id_comment_instagram` FROM `user_comm` WHERE `id_comment_instagram` in(";

		for ($i=0; $i < count($array_comments); $i++) { 
			
			$id_comment_instagram = $array_comments[$i]['id'];
			$sql .= "'$id_comment_instagram',";

		}

		$sql = substr($sql, 0, -1);
		$sql .= ")";

		$query = $mysqli->query($sql);

		$id_array = [0];

		while ($row = $query->fetch_assoc()) {
			$id_array[] = $row['id_comment_instagram'];
		}

		$sql = "";

		for ($i=0; $i < count($array_comments); $i++) { 
			
			$id_comment_instagram = $array_comments[$i]['id'];

			if (in_array($id_comment_instagram, $id_array)) {
				continue;
			}

			$user = $array_comments[$i]['user'];
			$text = $array_comments[$i]['text'];
			$date = $array_comments[$i]['date'];

			$text = htmlentities($text, ENT_QUOTES);

			$sql .= "('$id_comment_instagram', '$user_id', '$user', '$id', CURRENT_TIMESTAMP, '0', '$text', '$date'),";

		}

		if(!$sql) {
			$answer['state'] = 'error';
			$answer['message'] = 'Не обнаружено ни одного комментария под постом!';
		}
		else{
			$sql = substr($sql, 0, -1);
			$sql = "INSERT INTO `user_comm` (`id_comment_instagram`, `user`, `account`, `id_request`, `date`, `state`, `text`, `date_comm`) VALUES $sql ";
			$return = $mysqli->query($sql);

			if (!$return) {
				$answer['state'] = 'error';
				$answer['message'] = 'Неизвестная ошибка!';
			}
		}
		
		echo $answer = json_encode($answer, JSON_UNESCAPED_UNICODE);
	
	}

}

?>