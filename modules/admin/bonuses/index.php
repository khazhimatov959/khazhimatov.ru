<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);



?>
<link rel="stylesheet" href="/modules/admin/bonuses/style/bonuses.css">
<div class="bonuses_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Бонусы</p>
		</div>
         <div class="main_block_bonuses">

             <div class="line">
                 <button class="button_operation">+ Добавить бонус</button>
             </div>

			<div class="table">
							
					<div class="hbody">
						<div class="rows flex">
                            <div class="column"><input type="checkbox"></div>
							<div class="column">Название бонуса</div>
							<div class="column">Описание бонуса</div>
							<div class="column">Дата создания</div>
							<div class="column">ID</div>
							<div class="column"></div>
						</div>
					</div>
							
					<div class="tbody">
						<?php

						// for ($i=0; $i < count($ratings); $i++) { 
						// 		$user = $ratings[$i]['user'];
						// 		$place = $ratings[$i]['place'];
						// 		$count_bonuses = $ratings[$i]['count_bonuses'];
				
						?> 
							<div class="rows content">
								
								<div class="row row_1 flex">
                                    <div class="column"><input type="checkbox"></div>
									<div class="column">10 бесплатных комментариев</div>
									<div class="column">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</div>
                                    <div class="column">14.06.2018</div>
									<div class="column">44554855685</div>
									<div class="column flex operation">
                                        <div class="element_delete">
                                            <img src="/img/delete.png" alt="">
                                        </div>
                                    </div>
								</div>
								
							</div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">10 дней бесплатно</div>
                                <div class="column">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">20 дней бесплатно</div>
                                <div class="column">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">20 бесплатных комментариев и 10 бесплатных дней</div>
                                <div class="column">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                <div class="column">10 бесплатных комментариев</div>
                                <div class="column">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

						<?php //} ?>

					</div>
							
				</div>

            <div class="line operation_block flex">
                <select name="" id="">
                    <option value="0">Удалить</option>
                    <option value="1">Принять</option>
                </select>
                <button>Принять</button>
            </div>

		</div>	

	</div>

</div>
<script src="/modules/admin/bonuses/js/bonuses.js"></script>