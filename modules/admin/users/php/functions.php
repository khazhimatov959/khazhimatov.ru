<?php
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

function load_task(){

	global $mysqli;
	$user_id = $_SESSION['user_id_commenter'];

	$sql = "SELECT `request`.`id`, `link`, `amount`, `date`, `thematics`, `theme_type` as `type` FROM `request` LIMIT 10";
	$query = $mysqli->query($sql);

	$task = [];

	while($row = $query->fetch_assoc()){

		$task[] = array('id' => $row['id'], 'link' => $row['link'], 'amount' => $row['amount'], 'date' => $row['date'], 'thematics' => $row['thematics'], 'type' => $row['type']);

	}

	return $task;

}

function load_time_my_requests(){

	global $mysqli;
	$user_id = $_SESSION['user_id_commenter'];

	date_default_timezone_set("Europe/Moscow");
	$date = date('Y-m-d H:i:s', strtotime('-2 hour'));

	$sql = "SELECT `request`.`id`, `request`.`date`, (`amount_comm`.`count` - (`amount_requests_comm`.`sum` + '3')) as `amount_comm` FROM `request`, (SELECT SUM(`amount`) as `sum` FROM `request` WHERE `author` = '$user_id' and `date` > '$date') as `amount_requests_comm`, (SELECT COUNT(`id`) as `count` FROM `user_comm` WHERE `user` = '$user_id' and `date` > '$date') as `amount_comm` WHERE `request`.`author` = '$user_id' and `date` > '$date'";
	$query = $mysqli->query($sql);

	$ostatok_time = array();

	if($row = $query->fetch_assoc()){

		$amount_comm = $row['amount_comm'];

		$time_ostatok = strtotime($row['date'] . " +2 hour") - time();
		$hour = intval( $time_ostatok / (60 * 60) );
		$minutes = intval( ( $time_ostatok - $hour * 60 * 60 ) / 60 );
		$seconds =  intval( $time_ostatok - $hour * 60 * 60 - $minutes * 60 );

		if ($hour < 10) {
			$hour = '0' . $hour;
		}
		if ($minutes < 10) {
			$minutes = '0' . $minutes;
		}
		if ($seconds < 10) {
			$seconds = '0' . $seconds;
		}

		$time_ostatok = $hour . ":" . $minutes . ":" . $seconds;

		$ostatok_time = array('id' => $row['id'], 'amount_comm' => abs($amount_comm), 'ostatok_time' => $time_ostatok);

	}

	return $ostatok_time;

}

function load_my_comm(){

	global $mysqli;
	$query = $mysqli->query("SET SESSION group_concat_max_len = 100000;");

	$user_id = $_SESSION['user_id_commenter'];

	$sql = "SELECT `request`.`id`, `link`, `request`.`date`, `request`.`thematics`, `theme_type` as `type`, GROUP_CONCAT( CONCAT(`user_comm`.`account`, '959--959', `user_comm`.`text`, '959--959', `user_comm`.`date_comm`) SEPARATOR '959,959' ) as `comm` FROM `request` LEFT JOIN `user_comm` ON `request`.`id` = `user_comm`.`id_request` WHERE `user_comm`.`user` = '$user_id' GROUP BY `request`.`id` ORDER BY `request`.`date` DESC LIMIT 30";
	$query = $mysqli->query($sql);

	$requests = [];

	while($row = $query->fetch_assoc()){

		$comm = explode('959,959', $row['comm']);

		$comm_array = [];

		for ($i=0; $i < count($comm); $i++) {

			if (!$comm[$i]) continue;

			$explode = explode('959--959', $comm[$i]); 

			$profile = $explode[0];
			$text = $explode[1];
			$date = $explode[2];

			if (!array_key_exists($profile, $comm_array)) {
					$comm_array[$profile] = [];
			}
		
			$comm_array[$profile][] = array('text' => $text, 'date' => $date);

		}

		$requests[] = array('id' => $row['id'], 'link' => $row['link'], 'thematics' => $row['thematics'], 'type' => $row['type'], 'date' => $row['date'], 'comm' => $comm_array);

	}

	return $requests;
}