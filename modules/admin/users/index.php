<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);



?>
<link rel="stylesheet" href="/modules/admin/users/style/users.css">
<div class="users_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Пользователи</p>
		</div>
			
		<div class="main_block_users">

            <div class="line line_count flex">

                <div class="count_user flex">
                    <div class="count_user_text">
                        <p class="title">Пользователей всего</p>
                    </div>
                    <div class="count_user_number title">
                        <p>10287</p>
                    </div>
                </div>

                <div class="count_user flex">
                    <div class="count_user_text">
                        <p class="title">Авторизованных сейчас</p>
                    </div>
                    <div class="count_user_number title">
                        <p>10287</p>
                    </div>
                </div>
            </div>

            <div class="line line_count flex">

               <button class="button_operation">+ Добавить пользователя</button>
                <button class="button_operation">Показать администраторов</button>
                <button class="button_operation">Показать новых</button>
                <button class="button_operation">Показать с неподтвержденными профилями</button>

            </div>

			
			<div class="table">
							
					<div class="hbody">
						<div class="rows flex">
                            <div class="column"><input type="checkbox"></div>
                            <div class="column">Логин</div>
                            <div class="column">Тип пользователя</div>
                            <div class="column">Активность</div>
                            <div class="column">Бан</div>
                            <div class="column">Тариф</div>
                            <div class="column">Последняя авторизация</div>
							<div class="column">ID</div>
							<div class="column"></div>
						</div>
					</div>
							
					<div class="tbody">
						<?php

						// for ($i=0; $i < count($ratings); $i++) { 
						// 		$user = $ratings[$i]['user'];
						// 		$place = $ratings[$i]['place'];
						// 		$count_user = $ratings[$i]['count_user'];
				
						?>

                        <div class="rows content">

                            <div class="row row_1 flex">
                                <div class="column"><input type="checkbox"></div>
                                    <div class="column">khzhimatov959@mail.ru</div>
                                <div class="column">Администратор</div>
                                <div class="column">Да</div>
                                <div class="column">-</div>
                                <div class="column">-</div>
                                <div class="column">14.06.2018</div>
                                <div class="column">44554855685</div>
                                <div class="column flex operation">
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>
                            </div>

                        </div>

							<div class="rows content">
								
								<div class="row row_1 flex">
                                    <div class="column"><input type="checkbox"></div>
									<div class="column">1</div>
									<div class="column">Администратор</div>
									<div class="column">Да</div>
									<div class="column">-</div>
									<div class="column">-</div>
									<div class="column">14.06.2018</div>
									<div class="column">44554855685</div>
									<div class="column flex operation">

                                    </div>
								</div>

                                <div class="flex hr_line">
                                    <div class="hr"><hr></div>
                                    <div class="element_delete">
                                        <img src="/img/delete.png" alt="">
                                    </div>
                                </div>

								<div class="row row_2 flex">
									<div class="column title"><p>Профиль для добавления коментариев:</p> </div>
									<div class="column">
										<p>@milayadasha</p>
									</div>
									
									<div class="column">
                                        <input type="checkbox"> Подтвержден
									</div>
								</div>

                                <div class="row row_2 flex">
                                    <div class="column title"><p>Профиль для получения коментариев:</p> </div>
                                    <div class="column">
                                        <p>@milayadasha</p>
                                    </div>

                                    <div class="column">
                                        <input type="checkbox"> Подтвержден
                                    </div>
                                </div>
								
							</div>

						<?php //} ?>

					</div>
							
				</div>

            <div class="line operation_block flex">
                <select name="" id="">
                    <option value="0">Удалить</option>
                    <option value="1">Принять</option>
                </select>
                <button>Применить</button>
            </div>

            <div class="line">
                <div class="block_page flex">
                    <div class="block_number flex">
                        <div class="page_number arrow_left">
                            <img src="/img/arrow_up.png" alt="">
                        </div>
                        <div class="page_number">
                            <p>1</p>
                        </div>
                        <div class="page_number">
                            <p>2</p>
                        </div>
                        <div class="page_number">
                            <p>3</p>
                        </div>
                        <div class="page_number">
                            <p>...</p>
                        </div>
                        <div class="page_number">
                            <p>5</p>
                        </div>
                        <div class="page_number arrow_right">
                            <img src="/img/arrow_up.png" alt="">
                        </div>
                    </div>
                    <div class="block_count_page">
                        <div class="text title">
                            Показывать по
                        </div>
                        <select name="" id="">
                            <option value="20">20</option>
                            <option value="40">40</option>
                            <option value="60">60</option>
                            <option value="80">80</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
            </div>

		</div>	

	</div>

</div>
<script src="/modules/admin/users/js/users.js"></script>