function countdown(){

	var stop = false;

	var time = $('.countdown p').text();

	if (!time) {
		stop = true;
	}

	time = time.split(':');
	hour = time[0];
	minute = time[1];
	second = time[2];

	if (second == '00') {

		if (minute == '00') {

			if (hour == '00') {

				stop = true;
			}

			hour--;
			minute = '60';

		}

		minute--;
		second = '60';

	}

	second--;

	hour = Number(hour);
	minute = Number(minute);
	second = Number(second);

	if (hour < 10) {
		hour = '0' + hour;
	}

	if (minute < 10) {
		minute = '0' + minute;
	}

	if (second < 10) {
		second = '0' + second;
	}

	if (stop) {
		$('.time_count').remove();
		clearInterval(timerId);
		return false;
	}

	$('.countdown p').text(hour + ':' + minute + ':' + second);

}

function comm_complete(id_comm){

	$.ajax({
		type: "POST",
		url: 'modules/comments/php/switch.php',
		data: {
			headerLocation: 'comm_complete',
			id: id_comm
		},
		dataType    : 'json',
		success: function(json)
		{

			showMessage(json['message']);

			if (json['state'] == 'success') {			
				location.reload(true);
			}
			
		},
		error: function(text){

			showMessage('Неизвестня ошибка');
			console.log(text);
			return false;
		}
	});

}

var timerId;

$(document).ready(function(){
	$('.request_form input[type=submit], .request_form button').click(function(event){
		event.preventDefault();
		return false;
	});

	timerId = setInterval(countdown, 1000);

});
