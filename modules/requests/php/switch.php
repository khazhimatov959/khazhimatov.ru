<?php
session_start();

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	
	header('Content-Type: text/html; charset=utf-8');

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/instagramFunctions.php";

if (isset($_POST['headerLocation'])) {
	$headerLocation = $_POST['headerLocation'];
}

if(!isset($headerLocation)){
	$headerLocation = '';
}

switch ($headerLocation) {

	case 'requestsCreate':

		$link = $_POST['link'];
		$thematics_input = $_POST['thematics_input'];
		$count_comment = $_POST['count_comment'];
		$type = $_POST['type'];

		requestsCreate($link, $thematics_input, $count_comment, $type);
		break;

}

function load_time_my_requests(){

	global $mysqli;
	$user_id = $_SESSION['user_id_commenter'];

	date_default_timezone_set("Europe/Moscow");
	$time = date('Y-m-d H:i:s', strtotime('-2 hour'));
	//$date = date('Y-m-d H:i:s', strtotime('-14 day'));

	$sql = "SELECT (`amount_comm`.`count` - (`amount_requests_comm`.`sum` + '3')) as `amount_comm` FROM `request`, (SELECT SUM(`amount`) as `sum` FROM `request` WHERE `author` = '$user_id' and `date` > '$time') as `amount_requests_comm`, (SELECT COUNT(`id`) as `count` FROM `user_comm` WHERE `user` = '$user_id' and `date` > '$time') as `amount_comm` WHERE `request`.`author` = '$user_id' and `date` > '$time'";
	$query = $mysqli->query($sql);

	$ostatok_time = array();

	if($row = $query->fetch_assoc()){

		$amount_comm = $row['amount_comm'];

		$time_ostatok = strtotime($row['date'] . " +2 hour") - time();
		$hour = intval( $time_ostatok / (60 * 60) );
		$minutes = intval( ( $time_ostatok - $hour * 60 * 60 ) / 60 );
		$seconds =  intval( $time_ostatok - $hour * 60 * 60 - $minutes * 60 );

		if ($hour < 10) {
			$hour = '0' . $hour;
		}
		if ($minutes < 10) {
			$minutes = '0' . $minutes;
		}
		if ($seconds < 10) {
			$seconds = '0' . $seconds;
		}

		$time_ostatok = $hour . ":" . $minutes . ":" . $seconds;

		$ostatok_time = array('id' => $row['id'], 'amount_comm' => abs($amount_comm), 'ostatok_time' => $time_ostatok);

	}

	return $ostatok_time;

}

function requestsCreate($link, $thematics_input, $count_comment, $type){

	global $mysqli;
	$user_id = $_SESSION['user_id_commenter'];

	$media_id = getMediaId($link);

	$answer = array('state' => 'success', 'message' => 'Запрос успешно создан!');

	if(!$link){
		$answer['state'] = 'error';
		$answer['message'] = 'Пустая ссылка!';
	}

	if (preg_match("/[^a-zA-Z0-9\/\.:]/", $link)) {
		$answer['state'] = 'error';
		$answer['message'] = 'В поле ввода ссылки должны содержаться только латинские символы!';
	}

	if (strlen($link) < 23) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком мало символов введено в поле "Ссылка на пост"!';
	}

	if (strlen($link) > 60) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком много символов введено в поле "Ссылка на пост"!';
	}

	if (strrpos($link, 'instagram.com/p/') === false) {
		$answer['state'] = 'error';
		$answer['message'] = 'Некорректная ссылка!';
	}

	if(!$thematics_input){
		$answer['state'] = 'error';
		$answer['message'] = 'Тематика не заполнена!';
	}

	if (strlen($thematics_input) < 3) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком мало символов введено в поле "Тематика"!';
	}
	
	if (strlen($thematics_input) > 100) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком много символов введено в поле "Тематика"!';
	}

	if ($count_comment > 5) {
		$count_comment = 5;
	}


	if($answer['state'] != 'error'){

		$sql = "INSERT INTO `request` (`state`, `author`, `amount`, `thematics`, `theme_type`, `date`, `link`, `media_id`) VALUES ('0', '$user_id', '$count_comment', '$thematics_input', '$type', CURRENT_TIMESTAMP, '$link', '$media_id')";
		$return = $mysqli->query($sql);
		
		if(!$return){
			$answer['state'] = 'error';
			$answer['message'] = 'Ошибка';
		}

	}

	echo $answer = json_encode($answer, JSON_UNESCAPED_UNICODE);

}

?>