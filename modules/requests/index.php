<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/functions.php";
	
	$checkTariff = checkTariff();

	if (!$checkTariff) {
		echo "Срок дейтсвия вашего тарифа истек, пожалуйста, пополните баланс, чтобы восстановить доступ к этой странице";
		exit();
	}

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/requests/php/functions.php";


?>
<link rel="stylesheet" href="/modules/requests/style/requests.css">
<div class="request_block body_wrapper">
	
	<div class="block_body">
		<div class="page_info">
			<p class="page_info_title">Главная страница</p>
			<hr>
		</div>
			
		<div class="main_block_request">

			<?php
				if (!$checkTariff) {
					echo "Срок дейтсвия вашего тарифа истек, пожалуйста, пополните баланс, чтобы восстановить доступ к этой странице";
					exit();
				}
			?>
			
			<div class="block_1 block">

				<div class="block_header">
					<p class="title">Создание запроса</p>
					<hr>
				</div>

				<div class="block_body">
					
					<div class="line">
						<p class="line_title title">Cсылка на пост</p>
					</div>

					<div class="line">
						<input type="text" class="link_instagram_input" placeholder="Вставьте сюда ссылку">
					</div>

					<div class="line_buttons">
						<button class="button_comm button_active" onclick="button_comm(this);" data-id='0'>Любые комментарии на тему</button>
						<button class="button_comm" onclick="button_comm(this);" data-id='1'>Дословные комментарии</button>
					</div>

					<div class="line">
						<input type="text" placeholder="Введите тематику" class="thematics">
					</div>

					<div class="line line_count_comment">
						<label for="count_comment">Количество комментариев: </label>
						<select name="count_comment" id="count_comment">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>

					<div class="are_submit">
						
						<div class="line_button_submit">
							<button class="button_add_request" onclick="requestsCreate(this);">Cоздать запрос и начать получение</button>
						</div>

						<div class="line_button_submit">
							<a href="http://khazhimatov.ru/?page=comments"><button class="button_distribute">Раздавать комментарии</button></a>
						</div>

					</div>

				</div>

			</div>

			<div class="block_2 block">
				
				<div class="block_header">
					<p class="title">Статус активных запросов</p>
					<hr>
				</div>
				
				<div class="load_requests">
					<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/load_requests.php"; ?>
				</div>

			</div>

		</div>	

	</div>

</div>
<script src="/modules/requests/js/requests.js"></script>