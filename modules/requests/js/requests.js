$(document).ready(function(){
	$('.request_form input[type=submit], .request_form button').click(function(event){
		event.preventDefault();
		return false;
	})
});

function button_comm(this_tag){

	$('.button_active').removeClass('button_active');
	$(this_tag).addClass('button_active');

	var line_buttons = $('.thematics').parent();
	var id = $('.button_active').attr('data-id');

	var html_link;

	switch(id){

		case '0': 
			html_link = '<input type="text" placeholder="Введите тематику" class="thematics">';
			break;

		case '1': 
			html_link = '<textarea placeholder="Введите дословный комментарий, который нужно отставить под постом" class="thematics"></textarea>';
			break;
			
	}
	
	line_buttons.html(html_link);

}

function requestsCreate(this_tag){

	var link = $(".link_instagram_input").val();
	var thematics_input = $(".thematics").val();
	var count_comment = $("#count_comment").val();

	if(!link){
		showMessage('Пустая ссылка!');
		return false;
	}

	if (!/[a-zA-Z0-9\/\.:]/.test(link)) {
		showMessage('В поле ввода ссылки должны содержаться только латинские символы!');
		return false;
	}

	if (link.length < 23) {
		showMessage('Слишком мало символов введено в поле "Ссылка на пост"!');
		return false;
	}

	if (link.length > 60) {
		showMessage('Слишком много символов введено в поле "Ссылка на пост"!');
		return false;
	}

	if (link.indexOf('instagram.com/p/') == -1) {
		showMessage('Некорректная ссылка!');
		return false;
	}

	if(!thematics_input){
		showMessage('Введите пожалуйста тематику!');
		return false;
	}

	if (thematics_input.length < 3) {
		showMessage('Слишком мало символов введено в поле "Тематика"!');
		return false;
	}

	if (thematics_input.length > 100) {
		showMessage('Слишком много символов введено в поле "Тематика"!');
		return false;
	}

	var type = $('.button_active').attr('data-id');

	if (!type) {
		location.reload();
	}

	$.ajax({
		type: "POST",
		url: 'modules/requests/php/switch.php',
		data: {
			headerLocation: 'requestsCreate',
			link: link,
			thematics_input: thematics_input,
			count_comment: count_comment,
			type: type
		},
		success: function(html)
		{

			console.log(html);

			if(!isJSON(html)){
				showMessage('Неизвестня ошибка');
				return false;
			}

			json = JSON.parse(html);

			if (json['state'] == 'error') {
				showMessage(json['message']);
			}
			else {
				showMessage(json['message']);				
				$('.load_requests').load('/parts/load_requests.php');  
			}
		},
		error: function(){
			showMessage('Неизвестня ошибка');
			return false;
		}
	});
	
}