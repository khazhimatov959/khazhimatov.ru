<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/ratings/php/functions.php";

?>
<link rel="stylesheet" href="/modules/ratings/style/ratings.css">
<div class="rating_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Рейтинги</p>
			<hr>
			<p class="page_info_text">В данном разделе показыны пользователи с наибольшем количеством разданных коментариев.<br>Таблица обновляется каждые 6 месяцев</p>
		</div>
			
		<div class="main_block_rating">

			<div class="block_time">
				
				<div class="block_time_header">
					<p>ДО СБРОСА РЕЗУЛЬТАТОВ ТАБЛИЦЫ РЕЙТИНГА ОСТАЛОСЬ</p>
				</div>

				<div class="block_time_body">
					<div class="block_time_count_body flex">
						<div class="block_time_count">
							<div class="count_number flex">
								<div class="number"><p>1</p></div>
								<div class="number"><p>2</p></div>
							</div>
							<div class="count_day">Дней</div>
						</div>
						<div class="block_time_count">
							<div class="count_number flex">
								<div class="number"><p>1</p></div>
								<div class="number"><p>2</p></div>
							</div>
							<div class="count_day">Дней</div>
						</div>
						<div class="block_time_count">
							<div class="count_number flex">
								<div class="number"><p>1</p></div>
								<div class="number"><p>2</p></div>
							</div>
							<div class="count_day">Дней</div>
						</div>
					</div>
				</div>
				
			</div>

			<div class="block_rating">
	
				<div class="table">
							
					<div class="hbody">
						<div class="row flex">
							<div class="column">Место</div>
							<div class="column">Пользователь</div>
							<div class="column">Количество отправленных комментариев</div>
						</div>
					</div>
							
					<div class="tbody">
						<div class="row content flex">
							<div class="column title"><p class="place_rating">1</p></div>
							<div class="column">usermail@mail.ru</div>
							<div class="column">8453</div>
						</div>
						<div class="row content flex">
							<div class="column title"><p class="place_rating">1</p></div>
							<div class="column">usermail@mail.ru</div>
							<div class="column">8453</div>
						</div>
						<div class="row content flex">
							<div class="column title"><p class="place_rating">1</p></div>
							<div class="column">usermail@mail.ru</div>
							<div class="column">8453</div>
						</div>
					</div>
							
				</div>
			</div>

		</div>	

	</div>

</div>
<script src="/modules/ratings/js/ratings.js"></script>