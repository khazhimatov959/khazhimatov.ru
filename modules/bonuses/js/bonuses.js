$(document).ready(function(){
	$('.request_form input[type=submit], .request_form button').click(function(event){
		event.preventDefault();
		return false;
	})
});

function new_profile(param){

	var login;

	switch(param){

		case 1:
			var login = $('#login_instagram_receive').val();
			break;

		case 2:
			var login = $('#login_instagram_distributing').val();
			break;

		default: 
			var login = '';
			break;
	}

	$.ajax({
		type: "POST",
		url: '/modules/profiles/php/switch.php',
		data: {
			headerLocation: 'new_profile',
			param: param,
			login: login
		},
		success: function(html)
		{
			console.log(html);
			if (html.indexOf('success') != -1) {
				location.reload(true);
			}
		}
	});
}

