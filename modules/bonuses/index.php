<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/bonuses/php/functions.php";

?>
<link rel="stylesheet" href="/modules/bonuses/style/bonuses.css">
<div class="profile_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Бонусы</p>
			<hr>
		</div>
			
		<div class="main_block_profile">
			
			<div class="block_1 block">

				<div class="body_block flex">

					<div class="block_img">
						<div class="oval">
							<img src="/img/partners_img.png" alt="">
						</div>
					</div>

					<div class="block_info">
						<p class="title">10 бесплатных комментариев</p>
						<pre class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</pre>
					</div>
				</div>

			</div>

			<div class="block_1 block">

				<div class="body_block flex">

					<div class="block_img">
						<div class="oval">
							<img src="/img/partners_img.png" alt="">
						</div>
					</div>

					<div class="block_info">
					<p class="title">10 дней бесплатно</p>
						<pre class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</pre>
					</div>
				</div>

			</div>

			<div class="block_1 block">

				<div class="body_block flex">

					<div class="block_img">
						<div class="oval">
							<img src="/img/partners_img.png" alt="">
						</div>
					</div>

					<div class="block_info">
						<p class="title">20 дней бесплатно</p>
						<pre class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</pre>
					</div>
				</div>

			</div>

			<div class="block_1 block">

				<div class="body_block flex">

					<div class="block_img">
						<div class="oval">
							<img src="/img/partners_img.png" alt="">
						</div>
					</div>

					<div class="block_info">
						<p class="title">20 бесплатных комментариев и 10 бесплатных дней</p>
						<pre class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.</pre>
					</div>
				</div>

			</div>

		</div>	

	</div>

</div>
<script src="/modules/bonuses/js/bonuses.js"></script>