<?php
session_start();

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/libs/file_load/file_load.php";

if (isset($_POST['headerLocation'])) {
	$headerLocation = $_POST['headerLocation'];
}

if(!isset($headerLocation)){
	$headerLocation = '';
}

switch ($headerLocation) {

	case 'feedback_submit':

		feedback_submit();
		break;

}

function check_captcha() {

	global $_POST;

	if (isset($_POST['g-recaptcha-response'])) {
	    
	    $url_to_google_api = "https://www.google.com/recaptcha/api/siteverify";
	    $secret_key = '6LcmBWwUAAAAAEmv4HrciJTWhu7-PTp20-Df0hck';
	    $query = $url_to_google_api . '?secret=' . $secret_key . '&response=' . $_POST['g-recaptcha-response'] . '&remoteip=' . $_SERVER['REMOTE_ADDR'];
	    $data = json_decode(file_get_contents($query));
	    
	    if ($data->success) {
	        return true;
	    }
	} 

	return false;

}

function feedback_submit(){

	global $mysqli, $_POST, $_GET, $_FILES;
	$user_id = $_SESSION['user_id_commenter'];

	$answer = array('state' => 'success', 'message' => 'Запрос успешно создан!');

	$theme = $_POST['theme'];
	$text = $_POST['text'];
	$personal_data = $_POST['personal_data'];

	if(!$personal_data and $answer['state'] != 'error'){
		$answer['state'] = 'error';
		$answer['message'] = 'Вы не дали согласие на обработку персональных данных';
	}

	if(!$theme and $answer['state'] != 'error'){
		$answer['state'] = 'error';
		$answer['message'] = 'Введите пожалуйста тему обращения';
	}

	if(!$text and $answer['state'] != 'error'){
		$answer['state'] = 'error';
		$answer['message'] = 'Введите пожалуйста текст обращения';
	}

	if (!check_captcha() and $answer['state'] != 'error') {
		$answer['state'] = 'error';
		$answer['message'] = 'Докажите пожалуйста, что Вы не робот';
	}

	if ($_FILES['files']['tmp_name'][0] != '' and $answer['state'] != 'error') {
		
		$files = load_files($_FILES);

		$files_id = [];

		if ($files) {
			$files_id = files_to_database($files);
		}

		if (!$files_id) {
			$answer['state'] = 'error';
			$answer['message'] = 'Не удалось загрузить файлы';
		}

	}
	else $files_id = false;
	

	$sql = "INSERT INTO `feedback` (`author`, `theme`, `text`) VALUES ('$user_id', '$theme', '$text');";

	$result = $mysqli->query($sql);
	
	if(!$result and $answer['state'] != 'error'){
		$answer['state'] = 'error';
		$answer['message'] = 'Неизвестная ошибка';
	}
	else {
		$last_id = $mysqli->insert_id;
	}

	if ($files_id and $answer['state'] != 'error') {

		$sql = "INSERT INTO `feedback_attachment` (`feedback`, `file`) VALUES ";

		for ($i=0; $i < count($files_id); $i++) { 

			$file_id = $files_id[$i];
			$sql .= "('$last_id', '$file_id'),";
		}

		$sql = substr($sql, 0, -1);
		$result = $mysqli->query($sql);

	}

	echo $answer = json_encode($answer, JSON_UNESCAPED_UNICODE);

}

?>