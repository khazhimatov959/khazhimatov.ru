var formData = new FormData();

$('#feedbackForm').change(function(e){
  	
  	e.preventDefault();

	var $that = $(this);
	formData = new FormData($that.get(0)); 
	console.log(formData);

 	return false;
});

function feedback_submit(this_tag){

	var  theme = $('input[name="theme"]').val();
	var  text = $('textarea[name="text"]').val();
	var captcha = grecaptcha.getResponse();
	var  personal_data = $('input[name="personal_data"]').is(':checked');

	if (!theme) {
		showMessage("Введите пожалуйста тему обращения");
		return false;
	}

	if (!text) {
		showMessage("Введите пожалуйста текст обращения");
		return false;
	}

	if (!captcha.length) {
		showMessage("Докажите пожалуйста, что вы не робот");
		return false;
	}

	if(!personal_data){
		showMessage("Вы не дали согласие на обработку персональных данных");
		return false;
	}

   	formData.append('headerLocation', 'feedback_submit' );

	$.ajax({
		url         : 'modules/feedback/php/switch.php',
		type        : 'POST', 
		data        : formData,
		cache       : false,
		dataType    : 'json',
		processData : false,
		contentType : false, 
		success     : function( answer ){

			showMessage(answer['message']);

		},
		error: function( jqXHR, status, errorThrown ){
			showMessage('Неизвестная ошибка');
			console.log(jqXHR);
		}

	});

};