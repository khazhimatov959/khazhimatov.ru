	<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/feedback/php/functions.php";

?>
<link rel="stylesheet" href="/modules/feedback/style/feedback.css">
<div class="feedback_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Обращения</p>
			<hr>
		</div>
			
		<div class="main_block_feedback">
			
			<div class="block block_1">
				<h3><p>ЗАДАТЬ ВОПРОС В ПОДДЕРЖКУ</p></h3>
				<p>Обращения автоматически закрываются через 10 дней после ответа администрации, если пользователь не отреагирует</p>
				<div class="block_sub">
					<form action="" name="feedbackForm" id="feedbackForm" enctype="multipart/form-data">
					<div class="line flex">
						<div class="column_1">
							<p class="name_input">Тема обращения</p>
						</div>
						<div class="column_2">
							<input type="text" name="theme">
						</div>
					</div>

					<div class="line flex">
						<div class="column_1">
							<p>Текст обращения</p>
						</div>
						<div class="column_2">
							<textarea name="text" id="" rows="10"></textarea>
						</div>
					</div>

					<div class="line flex">
						
						<div class="column_1">
							
						</div>

						<div class="column_2">
							<div class="g-recaptcha" data-sitekey="6LcmBWwUAAAAAAHuHyXn74BtVZInWwc2smatdmEF"></div>
						</div>

					</div>

					<div class="line flex">
						
						<div class="column_1">
						
						</div>

						<div class="column_2">
							<p>Прикрепить файлы</p>
							<input type="button" onclick="$(this).next().click();" value="Обзор">
							<input type="file" name="files[]" id="files" multiple hidden>
						</div>
						
					</div>

					<div class="line flex">
						
						<div class="column_1">
							
						</div>

						<div class="column_2">
							<div class="line_check">
								<input type="checkbox" class="personal_data" name="personal_data">
								<p>Даю согласие на обработку персональных данных</p>
							</div>
						</div>
						
					</div>

					<div class="line flex">
						
						<div class="column_1">
							
						</div>

						<div class="column_2">
							<input type="submit" value="Отправить" name="submit" onclick="return feedback_submit(this);">
						</div>
						
					</div>
					</form>
				</div>
			</div>

			<div class="line_buttons">
				<button>Открытые обращения</button>
				<button>Закрытые обращения</button>
			</div>

			<div class="block_2">
				
				<?php

					$feedback_state = ['Открыто', 'Закрыты'];

					$feedback = load_feedback();

					for ($i=0; $i < count($feedback); $i++) { 
				?>
					<table class="feedback_table">
						<tr>
							<th>Дата</th>
							<th>Тема Обращения</th>
							<th class="right_line">Текст Обращения</th>
							<th>Статус</th>
						</tr>
						<tr>
							<td><?=$feedback[$i]['date']?></td>
							<td><?=$feedback[$i]['theme']?></td>
							<td class="right_line">
								<pre><?=$feedback[$i]['text']?>
								</pre>
							</td>
							<td><?=$feedback_state[$feedback[$i]['state']]?></td>
						</tr>
					</table>

				<? } ?>

			</div>
		</div>	

	</div>

</div>
<script src="/modules/feedback/js/feedback.js"></script>