<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/news/php/functions.php";

?>
<link rel="stylesheet" href="/modules/news/style/news.css">
<div class="news_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Новости</p>
			<hr>
		</div>
			
		<div class="main_block_news flex">
			
			<div class="main_block main_block_1">

				<div class="block">

					<div class="body_block flex">

						<div class="block_img">
							<img src="/img/news_test.png" alt="">
						</div>

						<div class="block_info">
							<p class="title">Компания представила новый облачный сервис</p>
							<pre class="content">Больше возможностей для постинга комментариев в сервисе.</pre>
							<div class="post_info flex">
								<p class="date">13.09.2017 20:10</p>
								<p> | </p> 
								<div class="view flex">
									<div class="view_icon"></div>
									<div class="view_number">
										<p>15</p>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="block">

					<div class="body_block flex">

						<div class="block_img">
							<img src="/img/news_test.png" alt="">
						</div>

						<div class="block_info">
							<p class="title">Компания представила новый облачный сервис</p>
							<pre class="content">Больше возможностей для постинга комментариев в сервисе.</pre>
							<div class="post_info flex">
								<p class="date">13.09.2017 20:10</p>
								<p> | </p> 
								<div class="view flex">
									<div class="view_icon"></div>
									<div class="view_number">
										<p>15</p>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="block">

					<div class="body_block flex">

						<div class="block_img">
							<img src="/img/news_test.png" alt="">
						</div>

						<div class="block_info">
							<p class="title">Компания представила новый облачный сервис</p>
							<pre class="content">Больше возможностей для постинга комментариев в сервисе.</pre>
							<div class="post_info flex">
								<p class="date">13.09.2017 20:10</p>
								<p> | </p> 
								<div class="view flex">
									<div class="view_icon"></div>
									<div class="view_number">
										<p>15</p>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="block">

					<div class="body_block flex">

						<div class="block_img">
							<img src="/img/news_test.png" alt="">
						</div>

						<div class="block_info">
							<p class="title">Компания представила новый облачный сервис</p>
							<pre class="content">Больше возможностей для постинга комментариев в сервисе.</pre>
							<div class="post_info flex">
								<p class="date">13.09.2017 20:10</p>
								<p> | </p> 
								<div class="view flex">
									<div class="view_icon"></div>
									<div class="view_number">
										<p>15</p>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="main_block main_block_2">
				<div class="block_popular_header">
					<div class="block_popular_title">
						<p class="title">Наиболее популярные</p>
						<hr>		
					</div>
				</div>
				
				<div class="block_popular_body">
					<div class="block_popular">
						<p class="block_name">Компания представила новый облачный сервис</p>
						<div class="post_info">
							<p class="date">13.09.2017 20:10</p>	
						</div>
					</div>
					
				</div>

				<div class="block_popular_body">
					<div class="block_popular">
						<p class="block_name">Компания представила новый облачный сервис</p>
						<div class="post_info">
							<p class="date">13.09.2017 20:10</p>	
						</div>
					</div>
					
				</div>
				
			</div>

		</div>	

	</div>

</div>
<script src="/modules/news/js/news.js"></script>