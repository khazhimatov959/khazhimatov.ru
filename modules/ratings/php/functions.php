<?php

function dateBetweenGenerate(){

	$time1 = strtotime(date('Y') . '-01-01 00:00:00');
	$time2 = strtotime(date('Y') . '-06-31 23:59:59');
	$time3 = strtotime(date('Y') . '-12-31 23:59:59');

	$curTime = time();

	if ($curTime > $time2) {
		$firstDate = date('Y-m-d H:i:s', $time2);
		$lastDate = date('Y-m-d H:i:s', $time3);
	}
	else {
		$firstDate = date('Y-m-d H:i:s', $time1);
		$lastDate = date('Y-m-d H:i:s', $time2);
	}

	return array($firstDate, $lastDate);
}

function load_ratings($startDate, $count = 12, $from = 'max'){

	global $mysqli;

	$user_id = $_SESSION['user_id_commenter'];

	$sql = "SET @n:=0;";

	$query = $mysqli->query($sql);
	
	$sql = "SELECT `user`, @n:=@n+1 as `place`, `user`.`email` as `user`, `count_comm`

				FROM (
						SELECT `user`, COUNT(DISTINCT `id_request`) AS `count_comm` 
							FROM `user_comm` WHERE `date` > '$startDate' GROUP BY `user`
					) 
					AS `rating` INNER JOIN `user` ON `rating`.`user` = `user`.`id`

				ORDER BY `count_comm` DESC 

				LIMIT $count
					
			";

	$query = $mysqli->query($sql);

	$rating = [];
	while ($row = $query->fetch_array()) {
		$rating[] = $row;
	} 

	return $rating;
}


?>