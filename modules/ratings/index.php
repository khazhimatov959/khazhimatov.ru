<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/ratings/php/functions.php";

	list($firstDate, $lastDate) = dateBetweenGenerate();
	$lastDate;
	$ratings = load_ratings($firstDate);

	$date = date('Y,m,d,H,i,s', strtotime($lastDate));
	$date = explode(',', $date);

	$date[1] = (int)$date[1] - 1;
	$date = implode(',', $date);

?>
<link rel="stylesheet" href="/modules/ratings/style/ratings.css">
<div class="rating_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Рейтинги</p>
			<hr>
			<p class="page_info_text">В данном разделе показыны пользователи с наибольшем количеством розданных коментариев.<br>Таблица обновляется каждые 6 месяцев</p>
		</div>
			
		<div class="main_block_rating">

			<div class="block_time">
				
				<div class="block_time_header">
					<p>ДО СБРОСА РЕЗУЛЬТАТОВ ТАБЛИЦЫ РЕЙТИНГА ОСТАЛОСЬ</p>
				</div>

				<div class="block_time_body">
					<div class="block_time_count_body flex">
						<div class="block_time_count days">
							<div class="count_number flex">
								<div class="number"><p class="timer_day_1">0</p></div>
								<div class="number"><p class="timer_day_2">0</p></div>
								<div class="number"><p class="timer_day_3">0</p></div>
							</div>
							<div class="count_day">Дней</div>
						</div>
						<div class="block_time_count">
							<div class="count_number flex">
								<div class="number"><p class="timer_hour_1">0</p></div>
								<div class="number"><p class="timer_hour_2">0</p></div>
							</div>
							<div class="count_day">Часов</div>
						</div>
						<div class="block_time_count">
							<div class="count_number flex">
								<div class="number"><p class="timer_minute_1">0</p></div>
								<div class="number"><p class="timer_minute_2">0</p></div>
							</div>
							<div class="count_day">Минут</div>
						</div>
					</div>
				</div>
				
			</div>

			<div class="block_rating">
	
				<div class="table">
							
					<div class="hbody">
						<div class="row flex">
							<div class="column">Место</div>
							<div class="column">Пользователь</div>
							<div class="column">Количество отправленных комментариев</div>
						</div>
					</div>
							
					<div class="tbody">
						<?php

						for ($i=0; $i < count($ratings); $i++) { 
								$user = $ratings[$i]['user'];
								$place = $ratings[$i]['place'];
								$count_comm = $ratings[$i]['count_comm'];
				
						?>
							<div class="row content flex">
								<div class="column title"><p class="place_rating"><?=$place?></p></div>
								<div class="column"><?=$user?></div>
								<div class="column"><?=$count_comm?></div>
							</div>
						
						<?php } ?>

					</div>
							
				</div>
			</div>

		</div>	

	</div>

</div>
<script src="/modules/ratings/js/ratings.js"></script>
<script>
	var date = new Date(<?=$date?>);
	timer(date);
</script>