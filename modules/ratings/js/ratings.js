
function new_profile(param){

	var login;

	switch(param){

		case 1:
			var login = $('#login_instagram_receive').val();
			break;

		case 2:
			var login = $('#login_instagram_distributing').val();
			break;

		default: 
			var login = '';
			break;
	}

	$.ajax({
		type: "POST",
		url: '/modules/profiles/php/switch.php',
		data: {
			headerLocation: 'new_profile',
			param: param,
			login: login
		},
		success: function(html)
		{
			console.log(html);
			if (html.indexOf('success') != -1) {
				location.reload(true);
			}
		}
	});
}

function timer(achiveDate = new Date(2018,07,21,1,52,15)) {

    var nowDate = new Date();

    var result = (achiveDate - nowDate) + 1000;

    if (result < 0) {

    	location.reload();
        return false;

    }

    var minutes = Math.floor((result/1000/60)%60);
    var hours = Math.floor((result/1000/60/60)%24);
    var days = Math.floor(result/1000/60/60/24);

    if (minutes < 10) minutes = '0' + minutes;
    if (hours < 10) hours = '0' + hours;
    if (days < 100) days = '0' + days;
    if (days < 10) days = '0' + days;


    days = days.toString();
    var day_1 = days[0];
    var day_2 = days[1];
    var day_3 = days[2];

    $('.timer_day_1').text(day_1);
    $('.timer_day_2').text(day_2);
    $('.timer_day_3').text(day_3);

    hours = hours.toString();
    var hour_1 = hours[0];
    var hour_2 = hours[1];

    $('.timer_hour_1').text(hour_1);
    $('.timer_hour_2').text(hour_2);

    minutes = minutes.toString();
    var minute_1 = minutes[0];
    var minute_2 = minutes[1];

    $('.timer_minute_1').text(minute_1);
    $('.timer_minute_2').text(minute_2);

    setTimeout(function(){timer(achiveDate)}, 60000);

}

function countdown(){
	
	console.log('22');

	var stop = false;

	var day = $('.timer_day_1').text() + $('.timer_day_2').text();
	var hour = $('.timer_hour_1').text() + $('.timer_hour_2').text();
	var minute = $('.timer_minute_1').text() + $('.timer_minute_2').text();

	if (minute == '00') {

		if (hour == '00') {

			if (day == '00') {

				stop = true;
			}

			day--;
			hour = '24';

		}

		hour--;
		minute = '60';

	}

	minute--;

	day = Number(day);
	hour = Number(hour);
	minute = Number(minute);

	if (day < 10) {
		day = '0' + day;
	}

	if (hour < 10) {
		hour = '0' + hour;
	}

	if (minute < 10) {
		minute = '0' + minute;
	}

	if (stop) {
		$('.time_count').remove();
		clearInterval(timerId);
		return false;
	}

	$('.countdown p').text(hour + ':' + minute + ':' + day);

}