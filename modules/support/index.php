	<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/support/php/functions.php";

?>
<link rel="stylesheet" href="/modules/support/style/support.css">
<div class="support_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Поддержка</p>
			<hr>
		</div>
			
		<div class="main_block_support flex">

			<div class="blocks">
				
				<div class="block block_1">
					<div class="info">
						<p>Telegram есть канал, в котором выходят анонсы мероприятий, вебинары, разборы обновлений и все остальное, что может быть полезным в работе, и пользовательский чат, где ты можешь задать любой вопрос, спросить совета или просто пообщаться с единомышленниками</p>
					</div>

					<div class="links">
						<div class="line flex">
							<div class="link_img">
								<img src="/img/channel_icon.png" alt="">
							</div>
							<div class="link_main">
								<div class="link_title">
									<p class='title'>Ссылка на канал</p>
								</div>
								<div class="link_main_text content">
									<a href="https://t.me/joinchat/AAAAAFKL0EIXVKVH38z3xQ">https://t.me/joinchat/AAAAAFKL0EIXVKVH38z3xQ</a>
								</div>
							</div>
						</div>
						<div class="line flex">
							<div class="link_img">
								<img src="/img/chat_icon.png" alt="">
							</div>
							<div class="link_main">
								<div class="link_title">
									<p class='title'>Ссылка на чат</p>
								</div>
								<div class="link_main_text content">
									<a href="https://t.me/joinchat/AAAAAFKL0EIXVKVH38z3xQ">https://t.me/joinchat/AAAAAFKL0EIXVKVH38z3xQ</a>
								</div>
							</div>
						</div>
						<div class="line flex">
							<div class="link_img">
								<img src="/img/video_icon.png" alt="">
							</div>
							<div class="link_main">
								<div class="link_title">
									<p class='title'> Видеоинструкция</p>
								</div>
								<div class="link_main_text content">
									<a href="https://t.me/joinchat/AAAAAFKL0EIXVKVH38z3xQ">https://t.me/joinchat/AAAAAFKL0EIXVKVH38z3xQ</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="block block_2 flex">
					<div class="sections_block ">
						<div class="line flex">
							<div class="sections_img">
								<img src="/img/user_icon.png" alt="">
							</div>
							<div class="sections_main">
								<div class="sections_title">
									<p class='title'> Профиль </p>
								</div>
							</div>
						</div>
						<div class="line flex">
							<div class="sections_img">
								<img src="/img/job_icon.png" alt="">
							</div>
							<div class="sections_main">
								<div class="sections_title">
									<p class='title'> Работа с сервисом </p>
								</div>
							</div>
						</div>
						<div class="line flex">
							<div class="sections_img">
								<img src="/img/job_jeneral_icon.png" alt="">
							</div>
							<div class="sections_main">
								<div class="sections_title">
									<p class='title'> Работа с системой в целом </p>
								</div>
							</div>
						</div>
						<div class="line flex">
							<div class="sections_img">
								<img src="/img/more_icon.png" alt="">
							</div>
							<div class="sections_main">
								<div class="sections_title">
									<p class='title'> Другое </p>
								</div>
							</div>
						</div>
					</div>
					<div class="sections_block ">
						<div class="line flex">
							<div class="sections_img">
								<img src="/img/user_icon.png" alt="">
							</div>
							<div class="sections_main">
								<div class="sections_title">
									<p class='title'> Профиль </p>
								</div>
							</div>
						</div>
						<div class="line flex">
							<div class="sections_img">
								<img src="/img/job_icon.png" alt="">
							</div>
							<div class="sections_main">
								<div class="sections_title">
									<p class='title'> Работа с сервисом </p>
								</div>
							</div>
						</div>
						<div class="line flex">
							<div class="sections_img">
								<img src="/img/job_jeneral_icon.png" alt="">
							</div>
							<div class="sections_main">
								<div class="sections_title">
									<p class='title'> Работа с системой в целом </p>
								</div>
							</div>
						</div>
						<div class="line flex">
							<div class="sections_img">
								<img src="/img/more_icon.png" alt="">
							</div>
							<div class="sections_main">
								<div class="sections_title">
									<p class='title'> Другое </p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="block block_3">
				<h3><p>ЗАДАТЬ ВОПРОС В ПОДДЕРЖКУ</p></h3>
				<p class="info">(задайте свой вопрос и получите ответ на электронную почту)</p>
				<div class="block_sub">
					<div class="line flex">
						<div class="column_1">
							<p class="name_input">Ваше ФИО</p>
						</div>
						<div class="column_2">
							<input type="text">
						</div>
					</div>

					<div class="line flex">
						<div class="column_1">
							<p class="name_input">Электронный адрес</p>
						</div>
						<div class="column_2">
							<input type="text">
						</div>
					</div>

					<div class="line flex">
						<div class="column_1">
							<p class="name_input">Телефон</p>
						</div>
						<div class="column_2">
							<input type="text">
						</div>
					</div>

					<div class="line flex">
						<div class="column_1">
							<p>Текст отзыва</p>
						</div>
						<div class="column_2">
							<textarea name="" id="" rows="6"></textarea>
						</div>
					</div>

					<div class="line flex">
						
						<div class="column_1">
							
						</div>

						<div class="column_2">
							<div class="g-recaptcha" data-sitekey="6LcmBWwUAAAAAAHuHyXn74BtVZInWwc2smatdmEF"></div>
						</div>

					</div>

					<div class="line flex">
						
						<div class="column_1">
						
						</div>

						<div class="column_2">
							<p>Прикрепить файл</p>
							<button>Обзор</button>
							<input type="file" hidden>
						</div>
						
					</div>

					<div class="line flex">
						
						<div class="column_1">
							
						</div>

						<div class="column_2">
							<div class="line_check">
								<input type="checkbox" class="personal_data">
								<p>Даю согласие на обработку персональных данных</p>
							</div>
						</div>
						
					</div>

					<div class="line flex">
						
						<div class="column_1">
							
						</div>

						<div class="column_2">
							<p>Прикрепить файл</p>
							<button>Обзор</button>
							<input type="file" hidden>
						</div>
						
					</div>

				</div>
			</div>

		</div>	

		<div class="block_question">
			
			<div class="line flex">
				<div class="question_img">
					<img src="/img/question_icon.png" alt="">
				</div>
				<div class="question_main">
					<div class="question_title">
						<p class='title'> Хочу передать права помощнику. Как я могу это сделать? </p>
					</div>
					<div class="question_main_text content">
						<pre class="content">
Дай помощнику логин и пароль от своего личного кабинета и профиля раздачи, и он сможет запрашивать все комментарии за тебя :)
Смотри, чтобы вкладка Рабочей панели была открыта только в одном браузере.
						</pre>
					</div>
				</div>
			</div>

			<div class="line flex">
				<div class="question_img">
					<img src="/img/question_icon.png" alt="">
				</div>
				<div class="question_main">
					<div class="question_title">
						<p class='title'> Хочу передать права помощнику. Как я могу это сделать? </p>
					</div>
					<div class="question_main_text content">
						<pre class="content">
Дай помощнику логин и пароль от своего личного кабинета и профиля раздачи, и он сможет запрашивать все комментарии за тебя :)
Смотри, чтобы вкладка Рабочей панели была открыта только в одном браузере.
						</pre>
					</div>
				</div>
			</div>

			<div class="line flex">
				<div class="question_img">
					<img src="/img/question_icon.png" alt="">
				</div>
				<div class="question_main">
					<div class="question_title">
						<p class='title'> Хочу передать права помощнику. Как я могу это сделать? </p>
					</div>
					<div class="question_main_text content">
						<pre class="content">
Дай помощнику логин и пароль от своего личного кабинета и профиля раздачи, и он сможет запрашивать все комментарии за тебя :)
Смотри, чтобы вкладка Рабочей панели была открыта только в одном браузере.
						</pre>
					</div>
				</div>
			</div>

			<div class="line flex">
				<div class="question_img">
					<img src="/img/question_icon.png" alt="">
				</div>
				<div class="question_main">
					<div class="question_title">
						<p class='title'> Хочу передать права помощнику. Как я могу это сделать? </p>
					</div>
					<div class="question_main_text content">
						<pre class="content">
Дай помощнику логин и пароль от своего личного кабинета и профиля раздачи, и он сможет запрашивать все комментарии за тебя :)
Смотри, чтобы вкладка Рабочей панели была открыта только в одном браузере.
						</pre>
					</div>
				</div>
			</div>

			<div class="line flex">
				<div class="question_img">
					<img src="/img/question_icon.png" alt="">
				</div>
				<div class="question_main">
					<div class="question_title">
						<p class='title'> Хочу передать права помощнику. Как я могу это сделать? </p>
					</div>
					<div class="question_main_text content">
						<pre class="content">
Дай помощнику логин и пароль от своего личного кабинета и профиля раздачи, и он сможет запрашивать все комментарии за тебя :)
Смотри, чтобы вкладка Рабочей панели была открыта только в одном браузере.
						</pre>
					</div>
				</div>
			</div>

			<div class="line flex">
				<div class="question_img">
					<img src="/img/question_icon.png" alt="">
				</div>
				<div class="question_main">
					<div class="question_title">
						<p class='title'> Хочу передать права помощнику. Как я могу это сделать? </p>
					</div>
					<div class="question_main_text content">
						<pre class="content">
Дай помощнику логин и пароль от своего личного кабинета и профиля раздачи, и он сможет запрашивать все комментарии за тебя :)
Смотри, чтобы вкладка Рабочей панели была открыта только в одном браузере.
						</pre>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>
<script src="/modules/support/js/support.js"></script>