<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/informational_base/php/functions.php";

?>
<link rel="stylesheet" href="/modules/informational_base/style/informational_base.css">
<div class="informational_base_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title title">База знаний</p>
			<hr>
		</div>
			
		<div class="main_block_informational_base flex">

			<div class="block block_1">

				<ul class="list_question">
					<li><p>Как быстро комментировать посты</p></li>
					<li><p>Когда не комментируют без Комментера</p></li>
					<li><p>Огненный конкурс</p></li>
					<li><p>Как раскачать комментариями пост в таргетированной рекламе</p></li>
					<li><p>Инста-миф #1 - Instagram не показывает комментарии короче 5-ти слов</p></li>
					<li><p>Комментер = теневой бан в Instagram?</p></li>
					<li><p>Полезные ресурсы для продвижения</p></li>
				</ul>

			</div>

			<div class="block block_2">
				
				<ul class="question">
					<li>
						<p>Как быстро комментировать посты</p>
						<pre class="answer_to_question">
Что греха таить, вопрос наболевший... Все думают, что Комментер, это история про то, как ты закинул денег и комментарии волшебным образом сыпаться на твой профиль в точное время (с нужным тебе интервалом), а качество этих комментов будет просто божественным.

Но, увы, такое бывает только в кино, где по ту сторону сидит несколько сотен людей на бешеной зарплате, которые круглосуточно комментируют посты во всем Instagram... И это возможно, но такие возможности есть только у Госдепа США или российского Кремля. Кстати, ходят слухи, что так оно у них все и происходи :)

В реальности живые комментарии, которые ты и получишь в нашем сервисе Commenter 2000, могут написать только живые и заинтересованные люди со своих профилей, а не боты или, упаси Бог, арабы или китайцы. Да, да, как бы это банально это не звучало :)

Но получить такие комменты можно, только если ты с начала отдашь той стороне точно такие же, настоящие комментарии. Со своего же аккаунта :)

Так вот, мы спросили пользователей Комментера, как им удается тратить на него максимум 15 минут в день, при этом раздавая крутые, а главное настоящие комменты :)


<b>Примеры вопросов</b>

	<img src="/img/answer1.png" alt=""> <img src="/img/answer2.png" alt="">
	<img src="/img/answer1.png" alt=""> <img src="/img/answer2.png" alt="">

						</pre>
					</li>
				</ul>

			</div>

		</div>	

	</div>

</div>
<script src="/modules/informational_base/js/informational_base.js"></script>