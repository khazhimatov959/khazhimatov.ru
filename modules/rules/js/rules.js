$(document).ready(function(){
	$('.request_form input[type=submit], .request_form button').click(function(event){
		event.preventDefault();
		return false;
	})
});

function new_profile(param){

	var login;

	switch(param){

		case 1:
			var login = $('#login_instagram_receive').val();
			break;

		case 2:
			var login = $('#login_instagram_distributing').val();
			break;

		default: 
			var login = '';
			break;
	}

	$.ajax({
		type: "POST",
		url: '/modules/profiles/php/switch.php',
		data: {
			headerLocation: 'new_profile',
			param: param,
			login: login
		},
		success: function(html)
		{
			console.log(html);
			if (html.indexOf('success') != -1) {
				location.reload(true);
			}
		}
	});
}

function showRulesProfile(profile){
	
	var text;

	switch(profile){
		
		case 1: 
			text = "<p class='title profile_info_title'>ПРОФИЛЬ ДЛЯ РАЗАЧИ КОММЕНТАРИЕВ</p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.";
			break;

		case 2: 
		text = "<p class='title profile_info_title'>ПРОФИЛЬ ДЛЯ ПОЛУЧЕНИЯ КОММЕНТАРИЕВ</p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.";
			break;
	}

	infoModal(text);

}

