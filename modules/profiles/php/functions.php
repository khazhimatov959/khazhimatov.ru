<?php


function load_profile(){

	global $mysqli;

	$user_id = $_SESSION['user_id_commenter'];

	$sql = "SELECT `id`, `login`, `type`, `state`  FROM `profile` WHERE `user` = '$user_id'";
	$query = $mysqli->query($sql);

	$profiles_recive = [];
	$profiles_distribute = [];

	while($row = $query->fetch_assoc()){

		$profile = array('id' => $row['id'], 'login' => $row['login'], 'state' => $row['state']);

		if ($row['type'] == '1') {
			$profiles_recive[] = $profile;
		}
		else{
			$profiles_distribute[] = $profile;
		}

	}

	return [$profiles_recive, $profiles_distribute];
}


?>