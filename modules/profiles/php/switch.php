<?php
session_start();

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/instagramFunctions.php";

if (isset($_POST['headerLocation'])) {
	$headerLocation = $_POST['headerLocation'];
}

if(!isset($headerLocation)){
	$headerLocation = '';
}

switch ($headerLocation) {

	case 'new_profile':

		$param = $_POST['param'];
		$login = $_POST['login'];

		new_profile($param, $login);
		break;

	case 'delete_profile':

		$id = $_POST['id'];

		delete_profile($id);
		break;

}

function new_profile($type, $login){

	global $mysqli;
	$user_id = $_SESSION['user_id_commenter'];

	$answer = array('state' => 'success', 'message' => 'Профиль привязан!');

	if ($type > 2 or $type < 1) {
		$type = 1;
	}

	$sql = "SELECT `id` FROM `profile` WHERE `login` = '$login' and `user` = '$user_id'";

	$query = $mysqli->query($sql);
	if($query->fetch_array()){

		$answer['state'] = 'error';
		$answer['message'] = 'Такой аккаунт уже был привязан ранее';

	}

	
	$instagram_id = LoadUserID($login);

	if (!$instagram_id and $answer['state'] != 'error') {

		$answer['state'] = 'error';
		$answer['message'] = 'Такого аккаунта не существует';
		
	}

    $return = false;

    if ($answer['state'] != 'error') {

        $sql = "INSERT INTO `profile` (`user`, `login`, `instagram_id`, `type`, `state`) VALUES ('$user_id', '$login', '$instagram_id', '$type', '0')";
        $return = $mysqli->query($sql);

    }

	if (!$return and $answer['state'] != 'error') {
		$answer['state'] = 'error';
		$answer['message'] = 'Неизвестная ошибка';
	}

	echo $answer = json_encode($answer, JSON_UNESCAPED_UNICODE);

}

function delete_profile($id){

	global $mysqli;
	$user_id = $_SESSION['user_id_commenter'];

	$answer = array('state' => 'success', 'message' => 'Привязанный профиль удален!');

	$sql = "SELECT `id` FROM `profile` WHERE `id` = '$id' and `user` = '$user_id'";

	$query = $mysqli->query($sql);

	if(!$query->fetch_array()){

		$answer['state'] = 'error';
		$answer['message'] = 'Ошибка';

	}
	
	if ($answer['message'] != 'Ошибка') {

		$sql = "DELETE FROM `profile` WHERE `profile`.`id` = '$id'";
		$return = $mysqli->query($sql);

		if (!$return) {
			$answer['state'] = 'error';
			$answer['message'] = 'Неизвестная ошибка';
		}
	}
	
	echo $answer = json_encode($answer, JSON_UNESCAPED_UNICODE);

}

?>