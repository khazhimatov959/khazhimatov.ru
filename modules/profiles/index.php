<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/profiles/php/functions.php";

	
	list($profiles_recive, $profiles_distribute) = load_profile();

	$arraySearch = [0,1,2];
	$arrayReplace = ['На проверке', 'Активен', 'Не прошел проверку'];

?>
<link rel="stylesheet" href="/modules/profiles/style/profiles.css">
<div class="profile_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Профили</p>
			<hr>
		</div>
			
		<div class="main_block_profile">
			
			<div class="block_1 block">

				<div class="block_header">
					<p>Профили для получения комментариев</p>
					<hr>
				</div>

				<div class="block_body">
					
					<div class="line line_profiles_reset flex">
						<button>Сбросить все профили</button>
						<p class="content">Стоимость сброса профилей для получения составляет 200 рублей</p>
					</div>

					<div class="line line_profile_link flex">
						<div><p class="title">@</p></div>
						<input type="text" class="link_instagram_input" id="login_instagram_receive" placeholder="Указывать без @">
						<button onclick="new_profile(1);">Привязать</button>
					</div>

					<div class="line">
						<hr>
					</div>

					<div class="profiles_distribute">
						<div class="table">

							<div class="row flex title">
								<div class="column">Имя</div>
								<div class="column">Статус</div>
								<div class="column">Действие</div>
							</div>
							
					<?php	for ($i=0; $i < count($profiles_recive); $i++) {  

								$id = $profiles_recive[$i]['id'];
								$login = $profiles_recive[$i]['login'];
								$state = str_replace($arraySearch, $arrayReplace, $profiles_recive[$i]['state']);
						?>
					
							<div class="row content flex">
								<div class="column title"><?=$login ?></div>
								<div class="column"><?=$state ?></div>
								<div class="column"><a href="" onclick="return delete_profile(<?=$id ?>);">Удалить</a></div>
							</div>

						<?php } ?>
							
						</div>
					</div>



				</div>

			</div>

			<div class="block_2 block">
				
				<div class="block_header">
					<p>Профиль для раздачи комментариев</p>
					<hr>
				</div>
	
				<div class="block_body">
					
					<div class="line line_profiles_distribute">
						<p class="content">*Профили на раздачу мы проверяем вручную, максимум за 24 часа. В Telegram тебе поступит сообщение об изменении статуса</p>
					</div>

					<div class="line line_profile_link flex">
						<div><p class="title">@</p></div>
						<input type="text" class="link_instagram_input" id="login_instagram_distributing" placeholder="Указывать без @">
						<button onclick="new_profile(2);" >Привязать</button>
					</div>

					<div class="line">
						<hr>
					</div>

					<div class="profiles_distribute">
						<div class="table">
							<div class="row flex title">
								<div class="column">Имя</div>
								<div class="column">Статус</div>
								<div class="column">Действие</div>
							</div>

						<?php	for ($i=0; $i < count($profiles_distribute); $i++) {  

								$id = $profiles_distribute[$i]['id'];
								$login = $profiles_distribute[$i]['login'];
								$state = str_replace($arraySearch, $arrayReplace, $profiles_distribute[$i]['state']);
						?>
					
							<div class="row content flex">
								<div class="column title"><?=$login ?></div>
								<div class="column"><?=$state ?></div>
								<div class="column"><a href="" onclick="return delete_profile(<?=$id ?>);">Удалить</a></div>
							</div>

						<?php } ?>
							
						</div>
					</div>



				</div>

			</div>

		</div>	

	</div>

</div>
<script src="/modules/profiles/js/profiles.js"></script>