$(document).ready(function(){
	$('.request_form input[type=submit], .request_form button').click(function(event){
		event.preventDefault();
		return false;
	})
});

function new_profile(param){

	var login;

	switch(param){

		case 1:
			var login = $('#login_instagram_receive').val();
			break;

		case 2:
			var login = $('#login_instagram_distributing').val();
			break;

		default: 
			var login = '';
			break;
	}

	if(!login){
		showMessage("Заполните пожалуйста поле ввода профиля");
		return false;
	}

	$.ajax({
		type: "POST",
		url: '/modules/profiles/php/switch.php',
		data: {
			headerLocation: 'new_profile',
			param: param,
			login: login
		},
		dataType: 'json',
		success: function( answer ){

			console.log(answer);
			showMessage(answer['message']);
			
			if (answer['state'] == 'success') {
				location.reload(true);
			}

		},
		error: function( text ){
			showMessage('Неизвестная ошибка');
			console.log(text);
		}
	});
}

function delete_profile(id){

	var question = confirm("Вы действительно хотите удалить этот профиль?");

	if(!id || !question){
		return false;
	}

	$.ajax({
		type: "POST",
		url: '/modules/profiles/php/switch.php',
		data: {
			headerLocation: 'delete_profile',
			id: id
		},
		dataType: 'json',
		success: function( answer ){

			console.log(answer);
			showMessage(answer['message']);
			
			if (answer['state'] == 'success') {
				location.reload(true);
			}

		},
		error: function( text ){
			showMessage('Неизвестная ошибка');
			console.log(text);
		}
	});

	return false;
}


