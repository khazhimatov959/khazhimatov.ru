<?php
session_start();

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/php/instagramFunctions.php";

if (isset($_POST['headerLocation'])) {
	$headerLocation = $_POST['headerLocation'];
}

if(!isset($headerLocation)){
	$headerLocation = '';
}

switch ($headerLocation) {

	case 'changePassword':

        $old_password = htmlentities($_POST['old_password'], ENT_QUOTES);
        $password_change = htmlentities($_POST['password_change'], ENT_QUOTES);
		changePassword($old_password, $password_change);

		break;

	case 'changeProfile':

        $email_change = htmlentities($_POST['email_change'], ENT_QUOTES);
        $name_change = htmlentities($_POST['name_change'], ENT_QUOTES);
        $surname_change = htmlentities($_POST['surname_change'], ENT_QUOTES);
        $phone_change = htmlentities($_POST['phone_change'], ENT_QUOTES);
        $telegram_change = htmlentities($_POST['telegram_change'], ENT_QUOTES);
        $reclame_change = htmlentities($_POST['reclame_change'], ENT_QUOTES);
        $notify_change = htmlentities($_POST['notify_change'], ENT_QUOTES);
		changeProfile($email_change, $name_change, $surname_change, $phone_change, $telegram_change, $reclame_change, $notify_change);

		break;

}

function changePassword($old_password, $password_change){

	global $mysqli;
	$user_id = $_SESSION['user_id_commenter'];

    $answer = array('state' => 'success', 'message' => 'Пароль успешно изменен!');
    
    if(!$password_change){
		$answer['state'] = 'error';
		$answer['message'] = 'Поле "Пароль" не заполнено!';
	}

	if (preg_match("/[^a-zA-Z0-9]/", $password_change)) {
		$answer['state'] = 'error';
		$answer['message'] = 'В поле ввода пароля должны содержаться только латинские символы!';
	}

	if (strlen($password_change) < 5) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком мало символов введено в поле "Пароль"!';
	}

	if (strlen($password_change) > 32) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком много символов введено в поле "Пароль"!';
    }
    

    if(!$old_password){
		$answer['state'] = 'error';
		$answer['message'] = 'Поле "Старый пароль" не заполнено!';
	}

	if (preg_match("/[^a-zA-Z0-9]/", $old_password)) {
		$answer['state'] = 'error';
		$answer['message'] = 'В поле ввода старого пароля должны содержаться только латинские символы!';
	}

	if (strlen($old_password) < 5) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком мало символов введено в поле "Старый пароль"!';
	}

	if (strlen($old_password) > 32) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком много символов введено в поле "Старый пароль"!';
    }

    if($answer['state'] != 'error'){

        $sql = "SELECT `password` FROM `user` WHERE `id` = '$user_id'";
        $query = $mysqli->query($sql);

        if ($result = $query->fetch_array()) {
            
            if(md5($old_password) != $result['password']) {
                $answer['state'] = 'error';
		        $answer['message'] = 'Неверный старый пароль!';
            }

        }
        else {
    
            $answer['state'] = 'error';
		    $answer['message'] = 'Ошибка!';

        }

    }
    
    if($answer['state'] != 'error'){

        $sql = "UPDATE `user` SET `password` = '" . md5($password_change) . "' WHERE `id` = '$user_id'";
	    $result = $mysqli->query($sql);

        if(!$result){
			$answer['state'] = 'error';
			$answer['message'] = 'Ошибка';
		}

    }
    
    echo $answer = json_encode($answer, JSON_UNESCAPED_UNICODE);

}

function changeProfile($email_change, $name_change, $surname_change, $phone_change, $telegram_change, $reclame_change, $notify_change){

	global $mysqli;
	$user_id = $_SESSION['user_id_commenter'];

    $answer = array('state' => 'success', 'message' => 'Изменения успешно сохранены!');

	if (!preg_match("/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-0-9A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u", $email_change)) {
		$answer['state'] = 'error';
		$answer['message'] = 'Неверный email!';
	}

	if(!$name_change){
		$answer['state'] = 'error';
		$answer['message'] = 'Поле "Имя" не заполнено!';
	}

	if (mb_strlen($name_change) < 2) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком мало символов введено в поле "Имя"!';
	}

	if (mb_strlen($name_change) > 15) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком много символов введено в поле "Имя"!';
	}

	if (mb_strlen($surname_change) < 2) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком мало символов введено в поле "Фамилия"!';
	}

	if (mb_strlen($surname_change) > 15) {
		$answer['state'] = 'error';
		$answer['message'] = 'Слишком много символов введено в поле "Фамилия"!';
	}

	if (preg_match("/[^0-9]/", $phone_change)) {
		$answer['state'] = 'error';
		$answer['message'] = 'Неверный телефон!';
	}

	if (!preg_match("/^[A-Za-z0-9_]{5,}$/", $telegram_change)) {
		$answer['state'] = 'error';
		$answer['message'] = 'Неверный логин телеграмма!';
	}

	if($answer['state'] != 'error'){

        $sql = "UPDATE `user` SET `name` = '$name_change', `surname` = '$surname_change', `email` = '$email_change', `phone` = '$phone_change', `telegram` = '$telegram_change', `notify` = '$notify_change', `reclame` = '$reclame_change' WHERE `user`.`id` = '$user_id'";
	    $result = $mysqli->query($sql);

        if(!$result){
			$answer['state'] = 'error';
			$answer['message'] = 'Ошибка';
		}

    }
    
    echo $answer = json_encode($answer, JSON_UNESCAPED_UNICODE);

}

?>