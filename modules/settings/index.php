<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/functions.php";

	$user_id = $_SESSION['user_id_commenter'];
	$sql = "SELECT `name`, `surname`, `email`, `phone`, `telegram`, `notify`, `reclame` FROM `user` WHERE `id` = $user_id";
	$query = $mysqli->query($sql);
	$row = $query->fetch_array();

	if ($row) {
		$name = $row['name'];
		$surname = $row['surname'];
		$email = $row['email'];
		$phone = $row['phone'];
		$telegram = $row['telegram'];
		$notify = $row['notify'];
		$reclame = $row['reclame'];
	}

?>
<link rel="stylesheet" href="/modules/settings/style/settings.css">
<div class="setting_block body_wrapper">
	
	<div class="block_title">
		
			

	</div>

	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Настройки профиля</p>
			<hr>
			<p class="page_info_text">В данном разделе вы можете изменить настройки профиля</p>
		</div>
			
		<div class="main_block_setting">
			
			<form action="" name="setting_form" class="setting_form">
				
				<div class="main_block_line line_avatar">
					
					<div class="above_avatar">
						<div class="avatar">
							<img src="/img/user.png" alt="">
						</div>
					</div>

					<div class="avatar_buttons">
						<input type="file" hidden class="ava_file">
						<input type="checkbox" hidden>
						<button class="avatar_delete">Удалить аватар</button>
						<button class="avatar_change" onclick="avatarChange(this);">Изменить аватар</button>
					</div>

				</div>

				<div class="main_block_line">
					<label for="email_change"><p>Почта</p></label>
					<input id="email_change" type="text" name="email" value="<?=$email?>" class="email_change" placeholder="Введите адрес электронной почты">
				</div>

				<div class="main_block_line">
					<label for="name_change"><p>Имя</p></label>
					<input id="name_change" type="text" name="name" value="<?=$name?>" class="name_change" placeholder="Введите имя">
				</div>

				<div class="main_block_line">
					<label for="surname_change"><p>Фамилия</p></label>
					<input id="surname_change" type="text" name="surname" value="<?=$surname?>" class="surname_change" placeholder="Введите фамилию">
				</div>

				<div class="main_block_line">
					<label for="phone_change"><p>Телефон</p></label>
					<input id="phone_change" type="text" name="phone" value="<?=$phone?>" class="phone_change" placeholder="Введите телефон">
				</div>

				<div class="main_block_line">
					<label for="telegram_change"><p>Телеграмм</p></label>
					<input id="telegram_change" type="text" name="telegram" value="<?=$telegram?>" class="telegram_change" placeholder="Введите логин телеграм">
				</div>

				<div class="main_block_line line_checked no-flex">
					<div class="line">
						<input type="checkbox" <?=checked($notify);?> name="notify" class="notify_change" id="notify_change">
						<label for="notify_change">Получать системные оповещения на почту</label>
					</div>

					<div class="line">
						<input type="checkbox" <?=checked($reclame);?> name="reclame" class="reclame_change" id="reclame_change">
						<label for="reclame_change">Получать рекламную рассылку на почту</label>
					</div>
				</div>

				
				<div class="main_block_line no-flex line_save">
					<input type="submit" name="save_setting" class="save_setting" value="СОХРАНИТЬ НАСТРОЙКИ" onclick="saveSetting();">
				</div>

				<div class="main_block_line line_password">
					<label for="password_change"><p>Изменить <br> текущий пароль</p></label>
					<input id="password_change" type="password" name="password" class="password_change" placeholder="Введите новый пароль">
					<input type="button" class="password_change_button" value="СМЕНИТЬ ПАРОЛЬ" onclick="changePasswordOpen();">
				</div>

			</form>


		</div>	

	</div>

</div>
<script src="/modules/settings/js/settings.js"></script>