$(document).ready(function(){
	$('.setting_form input[type=submit], .setting_form button').click(function(event){
		event.preventDefault();
		return false;
	})
})

function avatarChange(){
	$('.ava_file').click();
}

var modalPassword;

var password_change;

function changePasswordOpen(){

	password_change = $(".password_change").val();

	if(!password_change){
		showMessage('Поле "Пароль" не заполнено!');
		return false;
	}

	if (/[^a-zA-Z0-9]/.test(password_change)) {
		showMessage('В поле ввода пароля должны содержаться только латинские символы!');
		return false;
	}

	if (password_change.length < 5) {
		showMessage('Слишком мало символов введено в поле "Пароль"!');
		return false;
	}

	if (password_change.length > 32) {
		showMessage('Слишком много символов введено в поле "Пароль"!');
		return false;
	}

	if (!modalPassword) {

		modalPassword = new Modal(380, 70);
		modalPassword.element('.modalClass').css({'background-color': 'rgb(255, 223, 223)',
												'font-weight': 'bold'});

		modalPassword.element('.modal_content').css({'color': '#aa0000'});
		modalPassword.element('.modal_header').css({'background-color': '#ff0000'});
		modalPassword.element('.modal_footer').css({'background-color': '#ff0000'});
		modalPassword.footer('<button onclick="changePassword()">Подтвердить</button>');
		modalPassword.element('.modal_footer button').css({'background-color': '#aa0000'});
		modalPassword.title('Смена пароля');
	
	}

	modalPassword.content('');
	modalPassword.add_tag('<input type="password" placeholder="Введите старый пароль" class="old_password">', 'Старый пароль');
	modalPassword.add_tag('<input type="password" placeholder="Снова введите новый пароль" id="password_change_2">', 'Подтврждение нового пароля');
	modalPassword.open();

	return true;

}

function changePassword(){

	var old_password = $('.old_password').val();
	var password_change_2 = $('#password_change_2').val();

	if(password_change_2 != password_change){
		showMessage('Введенные пароли не совпадают!');
		return false;
	}

	if(!old_password){
		showMessage('Поле "Старый пароль" не заполнено!');
		return false;
	}

	if (/[^a-zA-Z0-9]/.test(old_password)) {
		showMessage('В поле ввода старого пароля должны содержаться только латинские символы!');
		return false;
	}

	if (old_password.length < 5) {
		showMessage('Слишком мало символов введено в поле "Старый пароль"!');
		return false;
	}

	if (old_password.length > 32) {
		showMessage('Слишком много символов введено в поле "Старый пароль"!');
		return false;
	}

	$.ajax({

		type: "POST",
		url: 'modules/settings/php/switch.php',
		data: {
			headerLocation: 'changePassword',
			old_password: old_password,
			password_change: password_change
		},
		success: function(html)
		{

			console.log(html);

			if(!isJSON(html)){
				showMessage('Неизвестня ошибка');
				return false;
			}

			json = JSON.parse(html);

			if (json['state'] == 'error') {
				showMessage(json['message']);
			}
			else {
				showMessage(json['message']);
				modalPassword.close();				 
			}
		},
		error: function(){
			showMessage('Неизвестня ошибка');
			return false;
		}

	});

}

function saveSetting(){

	var email_change = $('.email_change').val();
	var name_change = $('.name_change').val();
	var surname_change = $('.surname_change').val();
	var phone_change = $('.phone_change').val();
	var telegram_change = $('.telegram_change').val();
	password_change = $('.password_change').val();
	var reclame_change = $('#reclame_change').is(':checked');
	var notify_change = $('#notify_change').is(':checked');

	if (!/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-0-9A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u.test(email_change)) {
		showMessage('Неверный email!');
		return false;
	}

	if(!name_change){
		showMessage('Поле "Имя" не заполнено!');
		return false;
	}

	if (name_change.length < 2) {
		showMessage('Слишком мало символов введено в поле "Имя"!');
		return false;
	}

	if (name_change.length > 15) {
		showMessage('Слишком много символов введено в поле "Имя"!');
		return false;
	}

	if (surname_change.length < 2) {
		showMessage('Слишком мало символов введено в поле "Фамилия"!');
		return false;
	}

	if (surname_change.length > 15) {
		showMessage('Слишком много символов введено в поле "Фамилия"!');
		return false;
	}

	if (/[^0-9]/.test(phone_change)) {
		showMessage('Неверный телефон!');
		return false;
	}

	if (!/^[A-Za-z0-9_]{5,}$/.test(telegram_change)) {
		showMessage('Неверный логин телеграмма!');
		return false;
	}

	if (reclame_change) {
		reclame_change = 1;
	}
	else reclame_change = 0;

	if (notify_change) {
		notify_change = 1;
	}
	else notify_change = 0;

	$.ajax({

		type: "POST",
		url: 'modules/settings/php/switch.php',
		data: {
			headerLocation: 'changeProfile',
			email_change: email_change,
			name_change: name_change,
			surname_change: surname_change,
			phone_change: phone_change,
			telegram_change: telegram_change,
			reclame_change: reclame_change,
			notify_change: notify_change
		},
		success: function(html)
		{

			console.log(html);

			if(!isJSON(html)){
				showMessage('Неизвестня ошибка');
				return false;
			}

			json = JSON.parse(html);

			if (json['state'] == 'error') {
				showMessage(json['message']);
			}
			else {
				showMessage(json['message']);			 
			}
		},
		error: function(){
			showMessage('Неизвестня ошибка');
			return false;
		}

	});

}

