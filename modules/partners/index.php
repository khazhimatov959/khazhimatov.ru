<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/partners/php/functions.php";

?>
<link rel="stylesheet" href="/modules/partners/style/partners.css">
<div class="profile_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Партнеры</p>
			<hr>
			<p class="page_info_text">Выгоные предложения от наших партнеров</p>
		</div>
			
		<div class="main_block_profile">
			
			<div class="block_1 block">

				<div class="body_block flex">

					<div class="block_img">
						<div class="oval">
							<img src="/img/partners_img.png" alt="">
						</div>
					</div>

					<div class="block_info">
						<pre class="content">nstaSoft - инструмент для продвижения и автоматизации действий в Instagram. На сегодняшний день является лидером и превосходит всех своих конкурентов.

Сервис сочетает в себе набор полезных для продвижения опций, высокую скорость работы и круглосуточную техническую поддержку.

Регистрация по нашей ссылке, при оплате первого месяца даст 7 дней пользования сервисом в подарок и 30 потоков FastBoost для быстрой фильтрации.</pre>
					</div>
				</div>

				<div class="line">
					<button>Перейти</button>
				</div>

			</div>

			<div class="block_1 block">

				<div class="body_block flex">

					<div class="block_img">
						<div class="oval">
							<img src="/img/partners_img.png" alt="">
						</div>
					</div>

					<div class="block_info">
						<pre class="content">nstaSoft - инструмент для продвижения и автоматизации действий в Instagram. На сегодняшний день является лидером и превосходит всех своих конкурентов.

				Сервис сочетает в себе набор полезных для продвижения опций, высокую скорость работы и круглосуточную техническую поддержку.

				Регистрация по нашей ссылке, при оплате первого месяца даст 7 дней пользования сервисом в подарок и 30 потоков FastBoost для быстрой фильтрации.</pre>
					</div>
				</div>

				<div class="line">
					<button>Перейти</button>
				</div>

			</div>

			<div class="block_1 block">

				<div class="body_block flex">

					<div class="block_img">
						<div class="oval">
							<img src="/img/partners_img.png" alt="">
						</div>
					</div>

					<div class="block_info">
						<pre class="content">nstaSoft - инструмент для продвижения и автоматизации действий в Instagram. На сегодняшний день является лидером и превосходит всех своих конкурентов.

				Сервис сочетает в себе набор полезных для продвижения опций, высокую скорость работы и круглосуточную техническую поддержку.

				Регистрация по нашей ссылке, при оплате первого месяца даст 7 дней пользования сервисом в подарок и 30 потоков FastBoost для быстрой фильтрации.</pre>
					</div>
				</div>

				<div class="line">
					<button>Перейти</button>
				</div>

			</div>

		</div>	

	</div>

</div>
<script src="/modules/partners/js/partners.js"></script>