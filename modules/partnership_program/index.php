<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/partnership_program/php/functions.php";

?>
<link rel="stylesheet" href="/modules/partnership_program/style/partnership_program.css">
<div class="partnership_program_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title title">Партнерская программа</p>
			<hr>
		</div>
			
		<div class="main_block_partnership_program flex">

			<div class="block_1">

				<div class="block_level">
					
					<div class="block_info">

						<div class="block_info_title title">
							<p>Способы получить баллы</p>
						</div>

						<div class="block_info_sub flex">
							<div class="block_info_conditions">
								
								<div class="block_conditions_header flex">
									
									<div class="block_conditions_img">
										<img src="/img/standard.png" alt="">
									</div>

									<div class="block_conditions_title title">
										<p>Стандартные условия</p>
										<hr>
									</div>

								</div>

								<div class="block_info_text">
									<pre>
										• За первую оплату нового пользователя комиссия (вне зависимости от кол-ва дней) - 25%.
										• За каждую его последующую оплату - 10%.
									</pre>
								</div>
								
								<div class="block_info_what">
									<p>!Применяются автоматически</p>
								</div>

							</div>
							
							<div class="block_info_conditions">
								
								<div class="block_conditions_header flex">
									
									<div class="block_conditions_img">
										<img src="/img/special.png" alt="">
									</div>

									<div class="block_conditions_title title">
										<p>Специальные условия</p>
										<hr>
									</div>

								</div>

								<div class="block_info_text">
									<pre>
										Условия для «Крупных» партнеров, имеющих большие ресурсы с целевой аудиторией и, конкретно, имеющие от 200 рефералов и более:
										• За первую оплату нового пользователя комиссия- 33%.
										• За каждую его последующую оплату - 20%.
									</pre>
								</div>
								
								<div class="block_info_what">
									<p>!Применяются индивидуально - требуется обращение в техподдержку</p>
								</div>

							</div>

						</div>
						
						<div class="block_link">
							<p>Ваша уникальная ссылка</p>
							<div class="link_copy flex">
								<input type="text" class="link_text">
								<button class="button_copy_link">Скопировать</button>
							</div>

							<pre class="link_copy_info">
Рекомендуем видоизменять свою партнерскую ссылку при помощи сокращателей, например - bit.ly.
Что бы пользователям было удобно регистрироваться в партнерской программе через короткую ссылку
Не забывай периодически проверять эти укороченные ссылки на активность, со временем они устаревают и перестают работать.
							</pre>
						</div>

					</div>

				</div>

				<div class="block_level">
	
					<div class="table">
								
						<div class="hbody">
							<div class="row flex">
								<div class="column">Пользователь</div>
								<div class="column">Условия начисления</div>
								<div class="column">Дата начисления</div>
								<div class="column">Сумма</div>
							</div>
						</div>
								
						<div class="tbody">
							<div class="row content flex">
								<div class="column">usermail@mail.ru</div>
								<div class="column">Регистрация пользователя</div>
								<div class="column">20.02.2018</div>
								<div class="column">1000 р.</div>	
							</div>
							<div class="row content flex">
								<div class="column">usermail@mail.ru</div>
								<div class="column">Регистрация пользователя</div>
								<div class="column">20.02.2018</div>
								<div class="column">1000 р.</div>	
							</div>
							<div class="row content flex">
								<div class="column">usermail@mail.ru</div>
								<div class="column">Регистрация пользователя</div>
								<div class="column">20.02.2018</div>
								<div class="column">1000 р.</div>	
							</div>
						</div>
								
					</div>
					
				</div>

			</div>

			<div class="block_2">
				<div class="block_level">

					<div class="block_money">

						<div class="block_money_line flex">
							<p>Рефералов</p><div class="money_count"><p>0 руб.</p></div>
						</div>

						<div class="block_money_line flex">
							<p>Прибыль за регистрации:</p><div class="money_count"><p>0 руб.</p></div>
						</div>

						<div class="block_money_line flex">
							<p>Прибыль по оплатам:</p><div class="money_count"><p>0 руб.</p></div>
						</div>

						<div class="block_money_line flex">
							<p>Доступно для вывода:</p><div class="money_count"><p>0 руб.</p></div>
						</div>

						<div class="block_money_line flex">
							<p>Выведено: </p><div class="money_count"><p>0 руб.</p></div>
						</div>

						<button class="money_submit">ВЫВЕСТИ</button>

					</div>

				</div>

				<div class="block_level">
					<div class="block_text flex">
						<img src="/img/info_icon.png" height="45px">
						<div class="text">
							<pre>
- Не нужно искать партнеров с помощью прямого спама у блогеров, за это мы баним в системе без возврата денежных средств!
- Запрещено! При настройке контекстной рекламы, "гнать" брендовый трафик - по запросам, включающим название продукта, как на русском, так и на английском языке. За это то же наказание - бан в системе без возврата денежных средств!
							</pre>
						</div>
					</div>
				</div>

			</div>

		</div>	

	</div>

</div>
<script src="/modules/partnership_program/js/partnership_program.js"></script>