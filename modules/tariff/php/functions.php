<?php

function LoadTariff(){

	global $mysqli;

	$user_id = $_SESSION['user_id_commenter'];

	$sql = "SELECT `tariff_price` FROM `user_tariff` WHERE `user` = '$user_id' ORDER BY `id` DESC limit 1";
	$queryTariff = $mysqli->query($sql);
	$row = $queryTariff->fetch_array();
	$tariff_price = $row['tariff_price'];

	$sql = "SELECT `tariff`.`id`, `tariff`.`name`, `tariff`.`description`, (SELECT GROUP_CONCAT(concat(`tariff_price`.`id`, '/', `tariff_price`.`price`, '/', `tariff_price`.`day`) SEPARATOR ',') FROM `tariff_price` WHERE `tariff`.`id` = `tariff_price`.`tariff_id` ORDER BY `tariff_price`.`price`) as `price` FROM `tariff` GROUP BY `tariff`.`id`";
	$query = $mysqli->query($sql);

	$index = 0;

	while ($row = $query->fetch_assoc()) {
		
		$id = $row['id'];
		$name = $row['name'];
		$description = $row['description'];
		$price = $row['price'];

		$priceExplode = explode(',', $price);

		$price = [];

		$selectedTariff = false;

		for ($i=0; $i < count($priceExplode); $i++) { 

			$price_id_and_day = explode('/', $priceExplode[$i]);
			$price_id = $price_id_and_day[0];

			unset($price_id_and_day[0]);
			sort($price_id_and_day);

			$price_and_day = implode('/', $price_id_and_day);

			if ($price_and_day[0] == '0') {
				$money = 'Бесплатно';
			}
			else $money = $price_and_day;

			if ($tariff_price == $price_id) {

				$selectedTariff = $price_id;
				$selectedMoney = $money;

			}

			$price[] = array('id' => $price_id, 'money' => $money);

			
		}

		if (!$selectedTariff) {
			$selectedTariff = $price[0]['id'];
			$selectedMoney = $price[0]['money'];
			$classActivate = "";
			$buttonText = "Подключить";
		}
		else {
			$buttonText = "Подключен";
			$classActivate = "activeButton";
		}

		$index++;
		echo "<div class='block block_$index'>

				<div class='block_header'>
					<p>$name</p>
				</div>

				<div class='block_info'>
					<p>$description</p>
				</div>

				<div class='block_money'>
					<div id='dd' class='select_money wrapper-dropdown' tabindex='1'>
						<div class='money_body flex'>

							<div class='money_img'>
								<img src='/img/user.png' alt=''>
							</div> 

							<div class='money_name'>
								<p data-piceid='" . $selectedTariff . "'>" . $selectedMoney . " дней</p> 
							</div>

						</div> 
						<ul class='dropdown'>";

						for ($i=0; $i < count($price); $i++) { 
							echo "<li>
									<a href='#'> 
										<div class='money_body flex'>

											<div class='money_img'>
												<img src='/img/user.png' alt=''>
											</div> 

											<div class='money_name'>
												<p data-piceid='" . $price[$i]['id'] . "'>" . $price[$i]['money'] . " дней</p> 
											</div>

										</div> 
									</a>
								  </li>";
						}
						
				  echo "</ul>
					</div>

				</div>
				
				<div class='block_submit'>
					<button class='$classActivate' onclick='activateTariif(this)';>$buttonText</button>
				</div>

			</div>";

	}
}

?>