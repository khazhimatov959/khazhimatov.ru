<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

?>
<link rel="stylesheet" href="/modules/tariff/style/tariff.css">
<div class="tariff_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">Тарифы</p>
			<hr>
			<p class="page_info_text">В данном разделе вы можете выбрать подходящий тарифный план для работы в нашем сервисе</p>
		</div>
			
		<div class="main_block_tariff">
			
			<?php 

				require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/tariff/php/functions.php"; 
				LoadTariff();

			?>

		</div>	

	</div>

</div>
<script src="/modules/tariff/js/tariff.js"></script>