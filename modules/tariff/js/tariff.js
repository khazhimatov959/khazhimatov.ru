$(document).ready(function(){
	$('.request_form input[type=submit], .request_form button').click(function(event){
		event.preventDefault();
		return false;
	})

	$(".wrapper-dropdown li").on('mousedown', function(){

		var value = $(this.querySelector('.money_name p')).html();
		var id = $(this.querySelector('.money_name p')).attr('data-piceid');
		console.log(value);
		$(this).parent('ul').prev().children('.money_name').children('p').html(value).attr('data-piceid', id);
		
	});

});

function activateTariif(this_tag){

	var price_tag = $(this_tag).parent().prev().find('.money_name p')[0];
	var price = price_tag.innerText;
	var result = confirm("С вашего счета будет списано " + price + " рублей. Для продолжения нажмите Да!");

	if (result) {

		var price_id = price_tag.getAttribute('data-piceid');

		$.ajax({
			type: "POST",
			url: 'modules/tariff/php/switch.php',
			data: {
				headerLocation: 'activateTariif',
				price_id: price_id
			},
			success: function(html)
			{
				console.log(html);
				if (html.indexOf('success') != -1) {
					location.reload();
				}
			}
		});

	}

}

