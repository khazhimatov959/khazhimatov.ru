<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
	require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/block_cord.php";
	require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/comments/php/functions.php";

	ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/functions.php";
	
	$checkTariff = checkTariff();

	if (!$checkTariff) {
		echo "Срок дейтсвия вашего тарифа истек, пожалуйста, пополните баланс, чтобы восстановить доступ к этой странице";
		exit();
	}


?>
<link rel="stylesheet" href="/modules/comments/style/comments.css">
<link rel="stylesheet" href="/style/block_cord.css">
<div class="comment_block body_wrapper">
	
	<div class="block_body">
			
		<div class="page_info">
			<p class="page_info_title">КОММЕНТАРИИ</p>
			<hr>
		</div>
	
	<?php

	$ostatok_time = $amount_comm = '';

	$load_time_my_requests = load_time_my_requests();

	if (isset($load_time_my_requests['ostatok_time'])) {
		
		$ostatok_time = $load_time_my_requests['ostatok_time'];
		$amount_comm = $load_time_my_requests['amount_comm'];
		$id = $load_time_my_requests['id'];

	}

	?>

	<?php if ($ostatok_time): ?>

		<div class="time_count">
			
			<div class="time_count_block">
				<p>Запрос № <?=$id?></p>
			</div>

			<div class="time_count_block">

				<div class="time_count_title">
					<p>ОСТАЛОСЬ ВРЕМЕНИ</p>
				</div>

				<div class="time_count_content countdown">
					<p><?=$ostatok_time?></p>
				</div>
					
				<div class="time_count_title">
					<p>ОСТАЛОСЬ КОММЕНТАРИЕВ</p>
				</div>

				<div class="time_count_content">
					<p><?=$amount_comm?></p>
				</div>

			</div>
			<hr>
		</div>

		<?php endif ?>
			
		<div class="main_block_comment">
			
			<div class="over_block">
				<div class="block_name">
					<p>ОСТАВИТЬ КОММЕНТАРИИ</p>
				</div>

			<?php 

				$task = load_task();

				for ($i=0; $i < count($task); $i++) { 

					echo $block_cord;

					$type = $task[$i]['type'];
					$textCommentInfo = '';

					if ($type == 0) {
						$textCommentInfo = 'Тематика, на которую нужно оставить любые комментарии:';
					}
					else $textCommentInfo = 'Дословный текст комментария, который нужно оставить под постом:';

					$html_link = '0';

					switch($type){

						case '0': 
							$html_link = '<input type="text" placeholder="Введите тематику" class="thematics">';
							break;

						case '1': 
							$html_link = '<textarea placeholder="Введите дословный комментарий, который нужно отставить под постом" class="thematics"></textarea>';
						break;

					}

			?>

			

				<div class="block_1 block">
				
						<div class="comment_task">
							
							<div class="comment_task_title">
								<p class="title">Задание <?=$task[$i]['id'];?></p>
							</div>

							<div class="comment_task_info">
								<p class="title">Оставить комментарий с профиля раздачи</p>
								<a class="content" href="<?=$task[$i]['link'];?>"><?=$task[$i]['link'];?></a>
							</div>
							
							<div class="task_text_title">
								<p class="title"><?=$textCommentInfo;?></p>
							</div>

							<div class="task_text content">
								<pre><?=$task[$i]['thematics'];?></pre>
							</div>
							
							<div class="line">
								<button class="button_comm_сonfirm" onclick="comm_complete(<?=$task[$i]['id'];?>);">КОММЕНТАРИЙ НАПИСАН</button>
							</div>

							<div class="line">
								<button class="button_comm_сonfirm" onclick="comm_problem();">ПРОБЛЕМЫ С ПОСТОМ</button>
							</div>


						</div>

				</div>

			<?php 

				}

			?>

			</div>

			<div class="over_block comm_check">
				<div class="block_name">
					<p>КОММЕНТАРИИ НА ПРОВЕРКЕ</p>
				</div>

			<?php

				$load_my_comm = load_my_comm();

				for ($i=0; $i < count($load_my_comm); $i++) { 

					echo $block_cord;

					$type = $load_my_comm[$i]['type'];
					$textCommentInfo = '';

					if ($type == 0) {
						$textCommentInfo = 'Тематика, на которую нужно оставить любые комментарии:';
					}
					else $textCommentInfo = 'Дословный текст комментария, который нужно оставить под постом:';

			?>

				<div class="block_1 block">
				
						<div class="comment_task">
							
							<div class="comment_task_title">
								<p class="title">Задание <?=$load_my_comm[$i]['id']?></p>
							</div>

							<div class="comment_task_info">
								<p class="title">Оставить комментарий с профиля раздачи</p>
								<a class="content" href="<?=$load_my_comm[$i]['link']?>"><?=$load_my_comm[$i]['link']?></a>
							</div>
							
							<div class="task_text_title">
								<p class="title"><?=$textCommentInfo?></p>
							</div>

							<div class="task_text content">
								<pre><?=$load_my_comm[$i]['thematics']?></pre>
							</div>
							
							<?php

						$comm = $load_my_comm[$i]['comm'];

						foreach ($comm as $key => $value) {
				
					?>
						<div class="my_comment">
							
							<div class="my_comment_title">
								<p class="title">Мой комментарий</p>
							</div>
													
						<?php 

							$commArray = $value;
							$dateFirst = '';

							for ($x=0; $x < count($commArray); $x++) { 

								$text = html_entity_decode($commArray[$x]['text'], ENT_QUOTES);
								$date = $commArray[$x]['date'];
								$dateExplode = explode(' ', $date);
								$time = $dateExplode[1];
								$time = substr($time, 0, -3);


	
						?>
							<div class="comment_text content">
								<div class="comment_text_line">
									<div class="block_text"><p><?=$text ?></p></div>
									<div class="block_date"><p><?=$time ?></p></div>
								</div>
							</div>
								
						<?php 

							}

						?>

						</div>

					<?php }	?>

						</div>

				</div>
			
			<?php } ?>

			</div>

			<div class="block_comm overblock">
				
				<div class="block_name">
					<p>СТАТУС АКТИВНЫХ ЗАПРОСОВ</p>
				</div>
	
				<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/load_requests.php"; ?>

			</div>

		</div>	

	</div>

</div>
<script src="/modules/comments/js/comments.js"></script>