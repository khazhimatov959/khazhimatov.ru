<?php
	session_start();
	if (isset($_GET['code'])) {

		require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/verify.php";

	}
	else if (!isset($_SESSION['user_id_commenter'])) {

		require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/autorization.php";

	}
	else {

		if ($_SESSION['privilege'] == '1') {
			require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/admin/workspace.php";
		}
		else {
			require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/workspace.php";
		}
		
	}

?>
