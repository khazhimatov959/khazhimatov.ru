<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Commenter</title>
	<?php

		require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/head.php";
		require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
	?>
</head>
<body>
	
	<?php
		
		require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/header.php";
		require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/content.php";
		require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/footer.php";

	?>

</body>
</html>

