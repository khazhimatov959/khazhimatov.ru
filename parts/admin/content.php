<?php
$page = '';

if (isset($_GET['page'])) {
	$page = $_GET['page'];
}

switch ($page) {

    case 'profiles':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/profiles/index.php";
        break;

    case 'profiles':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/profiles/index.php";
        break;
        
    case 'tasks':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/tasks/index.php";
        break;

    case 'requests':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/requests/index.php";
        break;
	
	case 'working_panel':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/working_panel/index.php";
		break;

	case 'comments':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/comments/index.php";
		break;

    case 'new_comment':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/new_comment/index.php";
        break;

    case 'incoming_comments':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/incoming_comments/index.php";
        break;

    case 'outgoing_comments':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/outgoing_comments/index.php";
        break;

    case 'problematic_comments':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/problematic_comments/index.php";
        break;

    case 'archive_comments':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/archive_comments/index.php";
        break;

    case 'users':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/users/index.php";
        break;

	case 'tariff':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/tariff/index.php";
		break;

    case 'payments':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/payments/index.php";
        break;

    case 'news':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/news/index.php";
        break;

    case 'feedback':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/feedback/index.php";
        break;

    case 'support':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/support/index.php";
        break;

    case 'informational_base':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/informational_base/index.php";
        break;

    case 'partners':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/partners/index.php";
		break;

    case 'rules':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/rules/index.php";
        break;

    case 'bonuses':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/bonuses/index.php";
        break;

	case 'ratings':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/ratings/index.php";
		break;
		
	case 'news':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/news/index.php";
		break;

    case 'add_profile':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/add_profile/index.php";
        break;

    case 'add_tariff':
        require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/add_tariff/index.php";
        break;
	
	default:
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/admin/working_panel/index.php";
		break;
}


?>