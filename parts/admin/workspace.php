<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Commenter</title>
	<?php

		require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/admin/head.php";
		require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
	?>
</head>
<body>
	

	<div class="general">
		
		<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/admin/left_bar.php"; ?>

		<div class="right_bar">
	
		<?php

			require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/admin/header.php";
			require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/admin/content.php";

		?>

		</div>

	</div>

		<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/admin/footer.php"; ?>
	

</body>
</html>

