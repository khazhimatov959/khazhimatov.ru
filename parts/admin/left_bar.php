<div class="left_bar">
	<div class="left_bar_header">
		
		<div class="header_logo">

			<div class="logo_icon">
				
				<img src="/img/insta.png" alt="">

			</div>

			<div class="logo_name_text">

				<span>COMMENTER</span>

			</div>

		</div>

	</div>

	<div class="left_bar_menu">
		<ul class="menu_bar">

            <li class="flex">
                <div class="logo_icon">
                    <img src="/img/admin_workspace.png" alt="">
                </div>
                <div class="logo_name">
                    <a href="/index.php?page=profiles" class="profiles">
                        <div class="logo_name_text">
                            <p>Профили</p>
                        </div>
                    </a>
                </div>
            </li>

            <li class="flex">
                <div class="logo_icon">
                    <img src="/img/admin_tasks.png" alt="">
                </div>
                <div class="logo_name">
                    <a href="/index.php?page=tasks" class="tasks">
                        <div class="logo_name_text">
                            <p>Задания</p>
                        </div>
                    </a>
                </div>
            </li>

            <li class="flex">
                <div class="logo_icon">
                    <img src="/img/admin_pencil.png" alt="">
                </div>
                <div class="logo_name">
                    <a href="/index.php?page=requests" class="requests">
                        <div class="logo_name_text">
                            <p>Запросы</p>
                        </div>
                    </a>
                </div>
            </li>

			<li class="flex">
				<div class="logo_icon">
					<img src="/img/admin_workspace.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=working_panel" class="working_panel">
                        <div class="logo_name_text">
                            <p>Рабочая панель</p>
                        </div>
                    </a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/admin_pencil.png" alt="">
				</div>
                <div class="logo_name">
                    <a href="/index.php?page=comments" class="comments">
                        <div class="logo_name_text">
                            <p>Комментарии</p>
                        </div>
                    </a>
                    <ul class="sub_menu">
                        <a href="/index.php?page=new_comment" class="new_comment">
                            <li>
                                <p>Новые</p>

                            </li>
                        </a>
                        <a href="/index.php?page=incoming_comments" class="incoming_comments">
                            <li>
                                <p>Входящие</p>
                            </li>
                        </a>
                        <a href="/index.php?page=outgoing_comments" class="outgoing_comments">
                            <li>
                                <p>Исходящие</p>
                            </li>
                        </a>
                        <a href="/index.php?page=problematic_comments" class="problematic_comments">
                            <li>
                                <p>Проблемные</p>
                            </li>
                        </a>
                        <a href="/index.php?page=archive_comments" class="archive_comments">
                            <li>
                                <p>Архив</p>
                            </li>
                        </a>
                    </ul>
                </div>
                <div class="show_sub_menu">
                    <img src="/img/arrow_up.png" alt="">
                </div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/admin_users.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=users" class="users"><div class="logo_name_text">Пользователи</div></a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/admin_tariff.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=tariff" class="tariff"><div class="logo_name_text">Тарифы</div></a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/admin_money.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=payments" class="payments"><div class="logo_name_text">Оплаты</div></a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/admin_news.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=news" class="news"><div class="logo_name_text">Новости</div></a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/admin_feedback.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=feedback" class="feedback"><div class="logo_name_text">Обращения</div></a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/admin_support.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=support" class="support"><div class="logo_name_text">Поддержка</div></a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/informational_base.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=informational_base" class="informational_base"><div class="logo_name_text">База знаний</div></a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/partners.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=partners" class="partners"><div class="logo_name_text">Партнеры</div></a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/rules.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=rules" class="rules"><div class="logo_name_text">Правила сервиса</div></a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/bonuses.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=bonuses" class="bonuses"><div class="logo_name_text">Бонусы</div></a>
				</div>
			</li>
			<li class="flex">
				<div class="logo_icon">
					<img src="/img/ratings.png" alt="">
				</div>
				<div class="logo_name">
					<a href="/index.php?page=ratings" class="ratings"><div class="logo_name_text">Управление рейтингом</div></a>
				</div>
			</li>
		</ul>
	</div>

</div>