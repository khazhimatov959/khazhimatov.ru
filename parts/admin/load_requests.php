<?php
		if (!isset($_SESSION)) {
			session_start();
		}
?>
			<link rel="stylesheet" href="/style/load_request.css">
			<?php
				
				require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
				require_once $_SERVER['DOCUMENT_ROOT'] . "/php/functions.php";

				if (isset($_GET['page'])) {
					$page = $_GET['page'];
				}
				else $page = '';

				if($page == 'comments') {
					require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/block_cord.php";
				}

				if(!isset($block_cord))
							$block_cord = '';

				$requests = LoadRequests();

				for ($i=0; $i < count($requests); $i++) { 

					echo $block_cord;
			?>

				<div class="request_body">
					
					<div class="request_active">
						
						<div class="request_active_info">
							
							<p class="info_line">
								<span class="title">Номер: </span>
								<span class="content"><?=$requests[$i]['id']?></span>
							</p>

							<p class="info_line">
								<span class="title">Пост: </span>
								<span class="content"><a href="<?=$requests[$i]['link']?>"><?=$requests[$i]['link']?></a></span>
							</p>

							<p class="info_line">
								<span class="title">Получено </span>
								<span class="content"><?=$requests[$i]['amount_comm']?> из <?=$requests[$i]['amount']?></span>
							</p>

							<p class="info_line">
								<span class="title">Создан: </span>
								<span class="content"><?=$requests[$i]['date']?></span>
							</p>

						</div>
						
					<?php

						$comm = $requests[$i]['comm'];

						foreach ($comm as $key => $value) {
				
					?>
						<div class="request_comment">
							
							<div class="request_comment_title">
								<p class="title">Комментарий</p>
							</div>

							<div class="request_comment_info">
								<p class="title">Комментарий с профиля раздачи</p>
								<a class="content" href="https://instagram.com/<?=$key ?>">https://instagram.com/<?=$key ?></a>
							</div>
							
							<div class="comment_text_title">
								<p class="title">Текст комментария:</p>
							</div>
							
						<?php 

							$commArray = $value;
							$dateFirst = '';

							for ($x=0; $x < count($commArray); $x++) { 

								$text = html_entity_decode($commArray[$x]['text'], ENT_QUOTES);
								$date = $commArray[$x]['date'];
								$dateExplode = explode(' ', $date);
								$time = $dateExplode[1];
								$time = substr($time, 0, -3);


	
						?>
							<div class="comment_text content">
								<div class="comment_text_line">
									<div class="block_text"><p><?=$text ?></p></div>
									<div class="block_date"><p><?=$time ?></p></div>
								</div>
							</div>
								
							<?php 

								if (!$dateFirst) {
									$dateFirst = $date;
								}

							}

							if (date('Y-m-d H:i:s', strtotime($date . " +5 minute")) > date('Y-m-d H:i:s')): ?>
								
								<div class="line_buttons">
									<button class="button_comm_сonfirm">Подтвердить</button>
									<button class="button_comm_decline">Отклонить</button>
								</div>

							<?php endif ?>

						</div>

					<?php }	?>

					</div>

				</div>

				<? } ?>