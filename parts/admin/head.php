<link rel="icon" href="img/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="/style/admin/main.css">
<link rel="stylesheet" href="/style/admin/left_bar.css">
<link rel="stylesheet" href="/style/admin/header.css">
<link rel="stylesheet" href="/style/admin/footer.css">
<link rel="stylesheet" href="/libs/modal/modal.css">
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="/libs/modal/modal.js"></script>
<script src="/js/admin/main.js"></script>
<script>
  var loader = '<div class="lds-ripple"><div></div><div></div></div>';
</script>