<?php
$page = '';

if (isset($_GET['page'])) {
	$page = $_GET['page'];
}

switch ($page) {
	
	case 'settings':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/settings/index.php";
		break;

	case 'requests':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/requests/index.php";
		break;

	case 'profiles':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/profiles/index.php";
		break;

	case 'comments':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/comments/index.php";
		break;

	case 'tariff':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/tariff/index.php";
		break;

	case 'partners':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/partners/index.php";
		break;

	case 'bonuses':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/bonuses/index.php";
		break;

	case 'rules':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/rules/index.php";
		break;
		
	case 'partnership_program':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/partnership_program/index.php";
		break;

	case 'informational_base':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/informational_base/index.php";
		break;
		
	case 'support':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/support/index.php";
		break;
		
	case 'payment':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/payment/index.php";
		break;
		
	case 'ratings':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/ratings/index.php";
		break;
		
	case 'news':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/news/index.php";
		break;
	
	case 'feedback':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/feedback/index.php";
		break;

	case 'news_page':
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/news_page/index.php";
		break;
	
	default:
		require_once $_SERVER['DOCUMENT_ROOT'] . "/modules/profiles/index.php";
		break;
}


?>