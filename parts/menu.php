<link rel="stylesheet" href="/style/menu.css">
<script src="/js/menu.js"></script>
<div class="menu_up">
	<div class="menu_body">
		<ul>
			<li><a class="flex" href="/?page=requests"> <div class="li_icon"><img src="/img/home.png" alt=""></div> <div class="li_text"><p>Главная</p></div> </a></li>
			<li><a class="flex" href="/?page=profiles"> <div class="li_icon"><img src="/img/profiles.png" alt=""></div> <div class="li_text"><p>Профили</p></div></a> </li>
			<li><a class="flex" href="/?page=tariff"> <div class="li_icon"><img src="/img/tariff.png" alt=""></div><div class="li_text"><p>Тарифы</p></div> </a></li>
			<li><a class="flex" href="/?page=partners"> <div class="li_icon"><img src="/img/partners.png" alt=""></div><div class="li_text"><p>Партнеры</p></div> </a></li>
			<li><a class="flex" href="/?page=bonuses"> <div class="li_icon"><img src="/img/bonuses.png" alt=""></div><div class="li_text"><p>Бонусы</p></div> </a></li>
			<li><a class="flex" href="/?page=rules"> <div class="li_icon"><img src="/img/rules.png" alt=""></div><div class="li_text"><p>Правила сервиса</p></div> </a></li>
			<li><a class="flex" href="/?page=settings"> <div class="li_icon"><img src="/img/settings.png" alt=""></div> <div class="li_text"><p>Настройки пользователя</p></div></a></li>
			<li><a class="flex" href="/?page=partnership_program"> <div class="li_icon"><img src="/img/partnership_program.png" alt=""></div><div class="li_text"><p>Партнерская программа</p></div> </a></li>
			<li><a class="flex" href="/?page=informational_base"> <div class="li_icon"><img src="/img/informational_base.png" alt=""></div><div class="li_text"><p>База знаний</p></div> </a></li>
			<li><a class="flex" href="/?page=support"> <div class="li_icon"><img src="/img/support.png" alt=""></div><div class="li_text"><p>Поддержка</p></div> </a></li>
			<li><a class="flex" href="/?page=payment"> <div class="li_icon"><img src="/img/payment.png" alt=""></div><div class="li_text"><p>Оплата</p></div> </a></li>
			<li><a class="flex" href="/?page=ratings"> <div class="li_icon"><img src="/img/ratings.png" alt=""></div><div class="li_text"><p>Рейтинги</p></div> </a></li>
			<li><a class="flex" href="/?page=news"> <div class="li_icon"><img src="/img/news.png" alt=""></div><div class="li_text"><p>Новости</p></div> </a></li>
			<li><a class="flex" href="/?page=feedback"> <div class="li_icon"><img src="/img/feedback.png" alt=""></div><div class="li_text"><p>Обращения</p></div> </a></li>
		</ul>
	</div>
	<div class="button_exit">
		<button onclick="exit();">Выход</button>
	</div>
</div>