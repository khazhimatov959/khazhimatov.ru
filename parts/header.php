<?php
	
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";
	require_once $_SERVER['DOCUMENT_ROOT'] . "/php/functions.php";

	$balance = myBalance();

	list($place, $myComm, $meComm) = myRating();

?>
<header>
	<div class="header_block">

		<div class="header_logo">

			<div class="logo_icon">
				
				<img src="/img/insta.png" alt="">

			</div>

			<div class="logo_name">

				<span>COMMENTER</span>

			</div>

		</div>

		<div class="header_main">

			<div class="statictics">

				<div class="statictics_title">
					<span>СТАТИСТИКА</span>
				</div>

				<div class="statictics_body">

					<div class="statictics_main">

						<div class="statistics_count statictics_up">
							
							<span class="statictics_icon">
								<img width="12px" height="12px" src="/img/arrow.png" alt="">
							</span>

							<span class="statictics_number">
								<p><?=$myComm?></p>
							</span>

						</div>

						<div class="statistics_count statictics_down">
							
							<span class="statictics_icon">
								<img width="12px" height="12px" src="/img/arrow.png" alt="">
							</span>

							<span class="statictics_number">
								<p><?=$meComm?></p>
							</span>

						</div>

						<div class="statistics_count statictics_rating">
							
							<span class="statictics_icon">
								<img width="12px" height="12px" src="/img/rating.png" alt="">
							</span>

							<span class="statictics_number">
								<p><?=$place?></p>
							</span>

						</div>

					</div>

					<div class="statictics_info">
						
						<span class="statictics_icon" id="statictics_info_icon">
							?
						</span>

					</div>
				</div>
			</div>


			<div class="balance">
				
				<div class="balance_title">
					<p class="vertical_center">БАЛАНС</p>
				</div>

				<div class="balance_body">
					
					<div class="balance_number">
						<p class="vertical_center"><? echo $balance; ?> р.</p>
					</div>
					
					<hr class="vertical_line">
					
					<div class="balance_icon">
						
						<img src="/img/balance_icon.png" alt="">

					</div>

				</div>

			</div>

			<div class="user_block">
				
				<div class="user_info" onclick="show_menu();">
					
					<div class="user_icon">
						
						<img src="img/user_small.png" alt="">

					</div>

					<div class="user_name">
						<span>МОЯ УЧЕТНАЯ ЗАПИСЬ</span>
					</div>
					
				</div>

				<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/parts/menu.php"; ?>
				
			</div>
		</div>


	</div>
</header>