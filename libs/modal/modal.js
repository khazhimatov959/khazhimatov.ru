var loader = '<div class="lds-ripple"><div></div><div></div></div>';
var timer_close;
var timer_none;
function animation(element, animation, time, time_sleep = 0){

	var func_sleep;

	switch(animation){

		case 'scale_open': 

			clearTimeout(timer_none);
			clearTimeout(timer_close);

			time = time/1000;
			
			func_sleep = function(){

				element.css({	
								'animation-duration': time + 's',
								'animation-fill-mode': 'both',
								'animation-name': 'zoomIn',
								'a-webkit-animation-name': 'zoomIn'
							});

			};

			break;

		case 'scale_close':

			time = time/1000;

			func_sleep = function(){

				element.css({
							'animation-duration': time + 's',
							'animation-fill-mode': 'both',
							'animation-name': 'zoomOut',
							'a-webkit-animation-name': 'zoomOut'
						});

				timer_none = setTimeout(
							function(){ 
								element.addClass('none'); 
							}, 
							time * 1000
						);

			}; 
			
			break;

		case 'opacity_open': 

			clearTimeout(timer_none);
			clearTimeout(timer_close);
			
			time = time/1000;

			func_sleep = function(){

				element.css({
								'animation-duration': time + 's',
								'animation-fill-mode': 'both',
								'animation-name': 'fadeIn',
								'a-webkit-animation-name': 'fadeIn'
							});

			}; 

			break;

		case 'opacity_close': 
			
			time = time/1000;

			func_sleep = function(){

				element.css({
								'animation-duration': time + 's',
								'animation-fill-mode': 'both',
								'animation-name': 'fadeOut',
								'a-webkit-animation-name': 'fadeOut'
							});

				timer_none = setTimeout(
							function(){ 
								element.addClass('none'); 
							}, 
							time * 1000
						);
				

			}; 

			break;

	}

	if (time_sleep) {
		timer_close = setTimeout(func_sleep, time_sleep);
	}
	else func_sleep();

}

class Modal {
	constructor(width = 500, height = 300, left = "center", top = "center",
		
					modal_html = '<div class="nad_modal none">'+ 
							'	<div class="modalClass">'+ 
							'		<div class="modal_header">'+ 
							'			<div class="modal_title">Окно</div>' +
							'			<div class="modal_close">&times;</div>'+ 
							'		</div>'+ 
							'		<div class="modal_content">'+ 

							'		</div>'+ 
							'		<div class="modal_footer">'+ 
							'       	<button class="button_close">Ок</button>'+
							'		</div>'+ 
							'	</div>'+ 
							'</div>'

				)
	{

		this.modal_tag = $(modal_html);
		this.size(width, height)
		this.position(left, top);

		this.isDom = false;

		this.background = $('<div class="background"></div>');
		
	}
	
	open(time = 300, animation_param = 'scale', time_sleep = 0){
		
		var animation_param_open = animation_param + '_open';
		var animation_param_close = animation_param + '_close';
		
		if(!this.isDom){
			$('html').append(this.modal_tag);
			this.isDom = true;
		}

		this.element().removeClass('none');

		animation(this.modal_tag, animation_param_open, time, time_sleep);

		$('.modal_close, .button_close', this.modal_tag).click(function(){

			animation($(this).parents('.nad_modal'), animation_param_close, time);

		})

		this.reposition();

	}

    noClose(){
        this.element('.modal_close').remove();
    }

	close(time = 300, animation_param = 'scale', time_sleep = 0){

		var animation_param_close = animation_param + '_close';
		animation($(this.modal_tag), animation_param_close, time, time_sleep);

	}

	scroll(scroll = true){

		switch(scroll){

			case true: 
				scroll = 'scroll';
				break;

			case false: 
				scroll = 'hidden';
				break;

			case auto: 
				scroll = 'auto';
				break;

		}

		this.element('.modal_content').css({'overflow-y': scroll});
	}

	background(unset = true){

		if (true) {
			this.element().after(this.background);
			this.background.show();
		}
		else this.background.hide();
		
	}

	draggable(){
		$(this.modal_tag).draggable({containment: "window",  scroll: false, cancel:'.modal_content'});
	}

	title(text){
		var title_block = this.element('.modal_title');
		title_block.html(text);
	}

	content(text){
		var modal_content = this.element('.modal_content');
		modal_content.html(text);
	}

	size(width, height){

		if (width > screen.width - 50) {
			width = screen.width - 50;
		}

		if (height > screen.height - 50) {
			height = screen.height - 50;
		}

		this.width = width;
		this.height = height;

		this.resize();

	}

	resize(){

		var width = this.width;
		var height = this.height;

		var elementWidth = '';
		var elementHeight = '';

		if (width[width.length-1] == '%'){
			elementWidth = this.element();
		}
		else elementWidth = this.element('.modal_content');

		if (height[height.length-1] == '%'){
			elementHeight = this.element();
			height = scroll.height * Number(height.substring(0, height.length-1));
		}
		else elementHeight = this.element('.modal_content');

		
		elementWidth.css({'min-height': height});;
		elementHeight.css({'min-width': width});

		this.reposition();

	}

	max(width, height){

		var elementWidth = '';
		var elementHeight = '';

		if (width[width.length-1] == '%'){
			elementWidth = this.element();
		}
		else elementWidth = this.element('.modal_content');

		if (height[height.length-1] == '%'){
			elementHeight = this.element();
			height = scroll.height * Number(height.substring(0, height.length-1));
		}
		else elementHeight = this.element('.modal_content');

		
		elementWidth.css({'max-height': height});;
		elementHeight.css({'max-width': width});
		
	}

	position(left = 'center', top = 'center'){

		this.left = left;
		this.top = top;

		this.reposition();

	}

	reposition(){

		let left = this.left;
		let top = this.top;

		if(left == 'center'){
			left = 'calc(50% - ' + (this.width / 2) + 'px)';
		}
		if(top == 'center'){
			top = (screen.height / 2) - ((this.height + 210) / 2);
		}

		this.element().css({'top': top, 'left': left});

	}

	add_tag(tag_html ='', description = ''){

		var text =	 '<div class="nad_row">'+
						'<div class="input_title" data-text="'+description+'"><p>' + description + ': </p></div>'+
						 tag_html +
					'</div>';

		var modal_content = this.element('.modal_content');
			modal_content.append(text);
	}

	value(text){
		return $('.input_title[data-text="'+text+'"]').next().val();
	}

	add(text){
		var modal_content = this.element('.modal_content');
		modal_content.append(text);
	}

	footer(text){
		var modal_footer = this.element('.modal_footer');
		modal_footer.html(text);
	}

	footer_delete(){
		this.element('.modal_footer').remove();
	}

	header_delete(){
		this.element('.modal_header').remove();
	}

	element(tag = 'this'){
		
		if (tag == '.nad_modal' || tag == "this") {
			return $(this.modal_tag);
		}
		else return $(tag, this.modal_tag);
	}
}

class AlertClass extends Modal{

	constructor(width = 500, height = 300, left = "center", top = "center",
		
					modal_html = '<div class="nad_modal">'+ 
							'	<div class="modalClass">'+ 
							'		<div class="modal_header">'+ 
							'			<div class="modal_title">Окно</div>' +
							'			<div class="modal_close">&times;</div>'+ 
							'		</div>'+ 
							'		<div class="modal_content">'+ 

							'		</div>'+ 
							'		<div class="modal_footer">'+ 
							'       	<button class="button_close">Ок</button>'+
							'		</div>'+ 
							'	</div>'+ 
							'</div>'

				)
	{
		super(width, height, left, top,	modal_html);
	}

	alert(text){
		var sqrt = Math.ceil(Math.pow(text.length, 1/2));
		this.content('<textarea cols="' + (sqrt * 2) + '" rows="' + ( (sqrt / 2) + 1 ) + '">' + text + '</textarea>');
		this.open();
	}

}

var alertModal;

function Alert(text){

	if (!alertModal)
		alertModal = new AlertClass(100, 10);

	alertModal.alert(text);

}

	
class modalMessage extends Modal{

	constructor(width = 200, height = 20, left = "center", top = "center",
		
					modal_html = '<div class="nad_modal">'+ 
							'	<div class="modalClass">'+ 
							'		<div class="modal_content">'+ 

							'		</div>'+ 
							'	</div>'+ 
							'</div>'

				)
	{
		super(width, height, left, top,	modal_html);

		
		this.element().css({
			'padding-bottom' : '0px',
			'z-index': '999999999999',
			'border-radius': '30px',
			'box-shadow': '0 0 15px 2px #888'
		});

		this.element('.modal_content').css({
					'text-align': 'center',
					'color': '#fff',
					'padding': '10px 35px',
					'overflow': 'unset'
				});

		this.element('.modalClass').css({
					'background-color': '#666'
				});

	}

	open(time, time_sleep = 6000){

		time = time / 2;
		super.open(time, 'opacity');
		super.close(time, 'opacity', time_sleep);

	}

};

var messageModal;

function showMessage(text, left = 'center', bottom = '30',time = 1500, time_sleep = 4000){

	if (!messageModal)
		messageModal = new modalMessage(200, 20, 'center', 'unset');

	text = "<pre class='no_scroll'>" + text + "</pre>";
	messageModal.content(text);

	messageModal.size(50, 10);
	messageModal.open(time, time_sleep);

	messageModal.position(left, 'unset');

	messageModal.element().css({
								'bottom': bottom + 'px'
								});

	var width = messageModal.element().width();
	var height = messageModal.element().height() - 50;
	messageModal.size(width, height);

}

class modalInfo extends Modal{

	constructor(width = 200, height = 20, left = "center", top = "center",
		
					modal_html = '<div class="nad_modal">' + 
					'	<div class="modalClass">' + 
					'		<div class="modal_header">' + 
					'			<div class="modal_title">Окно</div>' +
					'			<div class="modal_close">&times;</div>' + 
					'		</div>' + 
					'		<div class="modal_content">' + 

					'		</div>' + 
					'	</div>' + 
					'</div>'

				)
	{
		super(width, height, left, top,	modal_html);
		this.max(width, height);
		
		this.element('.modalClass').css({
			'background-color': '#f1f1f1'
		});

		this.element().css({
							'padding-bottom' : '0px'
						});

		this.element('.modal_header').css({
		'background-color': '#f1f1f1'
		});

		this.element('.modal_close').addClass('modal_color');
	}

	open(time = 300){
		
		this.title('');
		super.open(time, 'opacity');

	}

}

var infoModalModal;

function infoModal(text, width = '1190', height = '100%', left = 'center', top = 0, newModal = false){

    if (newModal) {
        var modalNew = new modalInfo(width, height, left, top);
        modalNew.noClose();
        modalNew.content(text);
        modalNew.open();

        return false;
    }

    if(!infoModalModal){
        infoModalModal = new modalInfo(width, height, left, top);
        infoModalModal.noClose();
    }

    infoModalModal.content(text);
    infoModalModal.open();

}

class tooltipClass extends Modal{
	
	constructor(width = 200, height = 20, left = "center", top = "center",
		
					modal_html = '<div class="nad_modal">' + 
					'	<div class="modalClass">' + 
					'		<div class="modal_content">' + 
					'		</div>' + 
					'	</div>' + 
					'</div>'

				)
	{
		super(width, height, left, top,	modal_html);
		this.max(width, height);
		this.title('');

		this.element().css({
								'padding-bottom' : '0px',
								'z-index': '9999999999999',
								'border-radius': '5px',
								'border': '1px solid #000000'
							});
			
		this.element('.modalClass').css({
											'background-color': '#f1f1f1'
										});

		this.element('.modal_content').css({
											'padding': '10px'
										});

	}

	open(time = 400){

		super.open(time, 'opacity');
		
	}

	close(time = 400){
		super.close(time, 'opacity');
	}

}

var tooltipModal;

function tooltip(tag, text, distance = 5, left = 0, width = 400, height = 300){

	if(!tooltipModal){
		tooltipModal = new tooltipClass(width, height);
	}

    var thisElement = $(tag);

    tooltipModal.text = text;

	thisElement.hover(
		
		function(){

			var text = tooltipModal.text;

            if (text.charAt(0) == ':') {
                var attr = text.substring(1);
                var text = $(this).attr(attr);
            }

            tooltipModal.content(text);

			tooltipModal.open();
		
			var topModal = 0;
			var leftModal = 0;
		
			var widthWindow = $(window).width();
			
			var topElement = this.getBoundingClientRect().top;
			var leftElement = this.getBoundingClientRect().left;
			
			var widthElement = thisElement.width();
			var heightElement = thisElement.css('height');
			heightElement = Number(heightElement.substring(0, heightElement.length-2));

			var heightModal = tooltipModal.element().height();
			var widthModal = tooltipModal.element().width();
		
			topModal = topElement - heightModal - distance;
			
			if (topModal < 0) {
				topModal = topElement + heightElement + distance;
			}
		
			leftModal = leftElement + left;

			if((widthWindow - (leftModal + widthModal)) < 0){
				leftModal = widthWindow - widthModal - 5;
			}
			
			if (leftModal < 0) {
				leftModal = 0;
			}
		
			tooltipModal.position(leftModal, topModal);

		},

		function(){
			tooltipModal.close();
		}
		
	);

}
