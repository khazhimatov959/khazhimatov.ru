<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/php/sql.php";

class Load {
	
	private static $error = 0;
	public static $i = 0;
	public static function saveLoad($file){
		
		if(!self::isSecurity($file)){
			return false;
		}
		
		$path = self::getName($file['name'][self::$i]);
		
		$uploadfile = $_SERVER['DOCUMENT_ROOT'] . "/files/$path";
		$dirExplode = explode('/', $uploadfile);
		$dirExplode[count($dirExplode)-1] = '';

		
		$dir = implode('/', $dirExplode);

		mkdir($dir, 0777, true);

		if (move_uploaded_file($file['tmp_name'][self::$i],$uploadfile)) {
			return $path;
		}
	
		else{
			self::$error = 3;
			return false;
		}
	}

	private static function isSecurity($file){
		
		$blacklist = array(".php", ".phtml", ".php3", ".php4", ".html", ".htm");
		
		foreach ($blacklist as $item) {
			
			if (preg_match("/$item\$/i", $file['name'][self::$i])) {
				
				self::$error = 0;
				return false;

			}

		}

		$size = $file['size'][self::$i];
		
		if ($size > 524288000) {
			self::$error = 2;
			return false;	
		}

		return true;
	}

	private static function getName($filename){
		
		$name = md5(microtime());

		$name[4] = '/';
		$name[9] = '/';

		$name = substr($name, 0, 14);

		$name.=strrchr($filename, '.');
		
		$put = $_SERVER['DOCUMENT_ROOT'] . "/files/$name";

		if (!file_exists($put)) 
			return $name;

		return self::getName();

	}

	public static function getTextError($code){
		
		if ($code == 1)	
			return "Неверный тип";
		elseif ($code == 2) 
			return "Превышен максимальный размер загружаемого майла (5 МБ)";
		elseif ($code == 3) 
			return "Ошибка при загрузке. Попробуйте еще раз";

	}
}

function load_files($files){

	$files_array = [];

	if (isset($files['files'])) {
		
		for ($j=0; $j < count($files['files']['name']); $j++) { 
			
			Load::$i = $j;
			$path = Load::saveLoad($files['files']);
			
			if ($path) {
				
				$name = explode('.', $files['files']['name'][$j]);
				unset($name[count($name)-1]);
				$name = implode('.', $name);

				array_push($files_array, array('name' => $name, 'path' => $path));
		
			}

		}

	}

	return $files_array;

}

function files_to_database($files_array) {
	
	global $mysqli;

	$sql = "INSERT INTO `files` (`id`, `name`, `path`, `date`) VALUES ";

	for ($i=0; $i < count($files_array); $i++) { 
		
		$name = $files_array[$i]['name'];
		$path = $files_array[$i]['path'];
		$sql .= "(NULL, '$name', '$path', CURRENT_TIMESTAMP),";
	
	}

	$sql = substr($sql, 0, -1);
	$result = $mysqli->query($sql);

	if($result){

		$files_id =[];

		$last_id = $mysqli->insert_id;

		for ($i=0; $i < count($files_array); $i++) { 
			
			array_push($files_id, $last_id);
			$last_id--;

		}

		return $files_id;
	}

	return false;
}

?>