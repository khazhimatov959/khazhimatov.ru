<?

require_once "classInstagram.php";

function LoadUserID($username){

	$instagram = new Instagram($username, '');
	$instagram->loadUserId();

	return $instagram->user_id;

}


function getMediaId($url){

	$url = explode('?', $url);
	$url = $url[0];
	$url = explode('/', $url);

	if (!$url) return false;

	if (end($url)){
		$media_id = end($url);
	}
	else {
		$media_id = $url[count($url)-2];
	}

	return $media_id;

}

function LoadUserComments($username, $media_id){

	$instagram = new Instagram();
	$instagram->loadComments($media_id);
	$user_comments = $instagram->userComments($username);

	return $user_comments;

}

?>