<?php

function file_get_contents_curl($url) {

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);  
	$data = curl_exec($ch);
	curl_close($ch);

	return $data;
}

require_once "loadComments.php";

class Instagram {

	public $username = '';
	public $user_id = '';
	public $array_comments_username = [];
	public $array_comments = [];

	public function __construct($username = '', $user_id = ''){
		
		$this->username = $username;
		$this->user_id = $user_id;

	}

	public function loadUserId() {

		$url = "https://instagram.com/" . $this->username;
		$json_string = file_get_contents_curl($url);
		$json_string = explode('profilePage_', $json_string);
		$json_string = explode('",', $json_string[1]);
		$user_id = $json_string[0];

		$this->user_id = $user_id;

	}

	public function loadComments($media_id){

		if (empty($media_id)) {
			return false;
		}

		return $this->array_comments = loadComments($media_id);

	}

	public function userComments($username){

		$array_user_comments = [];

		$array_comments_username = $this->array_comments;

		foreach ($array_comments_username as $key => $value) {
			
			if ($value['user'] == $username) {
					
				$array_user_comments[] = array('date' => $value['created_at'], 'user' => $value['user'], 'text' => $value['text']);

			}

		}

		return $array_user_comments;

	}

}

?>