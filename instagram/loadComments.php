<?php 

function loadComments($mediaID){

	$r = shell_exec("curl 'https://www.instagram.com/p/". $mediaID ."/?__a=1' -H 'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' -H 'referer: https://www.instagram.com/p/". $mediaID ."/' -H 'authority: www.instagram.com' -H 'x-requested-with: XMLHttpRequest' --compressed");


	$json = json_decode($r, true);

	if( empty($json) ){
		exit('ERROR');
	}

	$edges = $json['graphql']['shortcode_media']['edge_media_to_comment']['edges'];

	if( empty($edges) ){
		exit('ERROR');
	}

	$comment_count = $json['graphql']['shortcode_media']['edge_media_to_comment']['count'];
	$loaded_count = count($edges);
	$end_cursor = $json['graphql']['shortcode_media']['edge_media_to_comment']['page_info']['end_cursor'];

	while ( $loaded_count < 40 && $loaded_count <= $comment_count ) {

		$instagram_gis = md5('fd64ef23f3eae3637322247180f4b6c0:{"shortcode":"'. $mediaID .'","first":50,"after":"'. $end_cursor .'"}');

		// var_dump($instagram_gis);

		$r = shell_exec("curl 'https://www.instagram.com/graphql/query/?query_hash=f0986789a5c5d17c2400faebf16efd0d&variables=%7B%22shortcode%22%3A%22". $mediaID ."%22%2C%22first%22%3A50%2C%22after%22%3A%22". $end_cursor ."%22%7D' -H 'accept-encoding: gzip, deflate, br' -H 'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36' -H 'referer: https://www.instagram.com/p/". $mediaID ."/' -H 'authority: www.instagram.com' -H 'x-requested-with: XMLHttpRequest' -H 'x-instagram-gis: {$instagram_gis}' --compressed");

		$json = json_decode( $r, true);

		$edges_second = $json['data']['shortcode_media']['edge_media_to_comment']['edges'];

		if( is_array($edges_second) ){
			$loaded_count += count($edges_second);
			$edges = array_merge($edges, $edges_second);
			$end_cursor = $json['data']['shortcode_media']['edge_media_to_comment']['page_info']['end_cursor'];
		}else{
			break;
		}

	}


	$R = array();


	foreach ($edges as $key => $edge) {
		$node = $edge['node'];

		if( isset( $R[ $node['id'] ] ) ){
			continue;
		}

		$R[ $node['id'] ] = array(
			'created_at' => date('Y-m-d H:i:s', $node['created_at'] ),
			'id' => $node['id'],
			'text' => $node['text'],
			'user' => $node['owner']['username']
		);

	}

	// Comparison function
	// function cmp($a, $b) {
	//     if ($a['created_at'] == $b['created_at']) {
	//         return 0;
	//     }
	//     return ($a['created_at'] > $b['created_at']) ? -1 : 1;
	// }

	//uasort($R, 'cmp');

	// foreach ($R as $key => $value) {

	// 	$date = $value['created_at'];
	// 	$user = $value['user'];
	// 	$text = $value['text'];

	// 	echo "<div>$date | $user: $text</div>";
	// }

	return $R;

}

?>